// MyBeamShape.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyBeamShape class.

#include "MyPackage/MyBeamShape.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyBeamShape class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(BeamShape, MyBeamShape,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyBeamShape::MyBeamShape(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : BeamShape() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Example initialization for beam parameters.

// void MyBeamShape::init( Settings& settings, Rndm* rndmPtrIn) {
// 
//   // Save pointer.
//   rndmPtr             = rndmPtrIn;
// 
//   // Main flags.
//   allowMomentumSpread = settings.flag("Beams:allowMomentumSpread");
//   allowVertexSpread   = settings.flag("Beams:allowVertexSpread");
//   if (settings.flag("Beams:allowVariableEnergy"))
//     allowMomentumSpread = false;
// 
//   // Parameters for beam A momentum spread.
//   sigmaPxA            = settings.parm("Beams:sigmaPxA");
//   sigmaPyA            = settings.parm("Beams:sigmaPyA");
//   sigmaPzA            = settings.parm("Beams:sigmaPzA");
//   maxDevA             = settings.parm("Beams:maxDevA");
// 
//   // Parameters for beam B momentum spread.
//   sigmaPxB            = settings.parm("Beams:sigmaPxB");
//   sigmaPyB            = settings.parm("Beams:sigmaPyB");
//   sigmaPzB            = settings.parm("Beams:sigmaPzB");
//   maxDevB             = settings.parm("Beams:maxDevB");
// 
//   // Parameters for beam vertex spread.
//   sigmaVertexX        = settings.parm("Beams:sigmaVertexX");
//   sigmaVertexY        = settings.parm("Beams:sigmaVertexY");
//   sigmaVertexZ        = settings.parm("Beams:sigmaVertexZ");
//   maxDevVertex        = settings.parm("Beams:maxDevVertex");
//   sigmaTime           = settings.parm("Beams:sigmaTime");
//   maxDevTime          = settings.parm("Beams:maxDevTime");
// 
//   // Parameters for beam vertex offset.
//   offsetX             = settings.parm("Beams:offsetVertexX");
//   offsetY             = settings.parm("Beams:offsetVertexY");
//   offsetZ             = settings.parm("Beams:offsetVertexZ");
//   offsetT             = settings.parm("Beams:offsetTime");
// 
// }

//--------------------------------------------------------------------------

// Example to set the two beam momentum deviations and the beam vertex.

// void MyBeamShape::pick() {
// 
//   // Reset all values.
//   deltaPxA = deltaPyA = deltaPzA = deltaPxB = deltaPyB = deltaPzB
//     = vertexX = vertexY = vertexZ = vertexT = 0.;
// 
//   // Set beam A momentum deviation by a three-dimensional Gaussian.
//   if (allowMomentumSpread) {
//     double totalDev, gauss;
//     do {
//       totalDev = 0.;
//       if (sigmaPxA > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPxA  = sigmaPxA * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaPyA > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPyA  = sigmaPyA * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaPzA > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPzA  = sigmaPzA * gauss;
//         totalDev += gauss * gauss;
//       }
//     } while (totalDev > maxDevA * maxDevA);
// 
//     // Set beam B momentum deviation by a three-dimensional Gaussian.
//     do {
//       totalDev = 0.;
//       if (sigmaPxB > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPxB  = sigmaPxB * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaPyB > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPyB  = sigmaPyB * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaPzB > 0.) {
//         gauss     = rndmPtr->gauss();
//         deltaPzB  = sigmaPzB * gauss;
//         totalDev += gauss * gauss;
//       }
//     } while (totalDev > maxDevB * maxDevB);
//   }
// 
//   // Set beam vertex location by a three-dimensional Gaussian.
//   if (allowVertexSpread) {
//     double totalDev, gauss;
//     do {
//       totalDev = 0.;
//       if (sigmaVertexX > 0.) {
//         gauss     = rndmPtr->gauss();
//         vertexX   = sigmaVertexX * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaVertexY > 0.) {
//         gauss     = rndmPtr->gauss();
//         vertexY   = sigmaVertexY * gauss;
//         totalDev += gauss * gauss;
//       }
//       if (sigmaVertexZ > 0.) {
//         gauss     = rndmPtr->gauss();
//         vertexZ   = sigmaVertexZ * gauss;
//         totalDev += gauss * gauss;
//       }
//     } while (totalDev > maxDevVertex * maxDevVertex);
// 
//     // Set beam collision time by a Gaussian.
//     if (sigmaTime > 0.) {
//       do gauss    = rndmPtr->gauss();
//       while (abs(gauss) > maxDevTime);
//       vertexT     = sigmaTime * gauss;
//     }
// 
//     // Add offset to beam vertex.
//     vertexX      += offsetX;
//     vertexY      += offsetY;
//     vertexZ      += offsetZ;
//     vertexT      += offsetT;
//   }
// 
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
