// MySigmaProcess.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MySigmaProcess class.

#include "MyPackage/MySigmaProcess.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MySigmaProcess class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(SigmaProcess, MySigmaProcess,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MySigmaProcess::MySigmaProcess(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : SigmaProcess() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Example set up allowed flux of incoming partons.
// addBeam: set up PDF's that need to be evaluated for the two beams.
// addPair: set up pairs of incoming partons from the two beams.

// bool MySigmaProcess::initFlux() {
// 
//   // Reset arrays (in case of several init's in same run).
//   inBeamA.clear();
//   inBeamB.clear();
//   inPair.clear();
// 
//   // Read in process-specific channel information.
//   string fluxType = inFlux();
// 
//   // Case with g g incoming state.
//   if (fluxType == "gg") {
//     addBeamA(21);
//     addBeamB(21);
//     addPair(21, 21);
//   }
// 
//   // Case with q g incoming state.
//   else if (fluxType == "qg") {
//     for (int i = -nQuarkIn; i <= nQuarkIn; ++i) {
//       int idNow = (i == 0) ? 21 : i;
//       addBeamA(idNow);
//       addBeamB(idNow);
//     }
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//     if (idNow != 0) {
//       addPair(idNow, 21);
//       addPair(21, idNow);
//     }
//   }
// 
//   // Case with q q', q qbar' or qbar qbar' incoming state.
//   else if (fluxType == "qq") {
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//     if (idNow != 0) {
//       addBeamA(idNow);
//       addBeamB(idNow);
//     }
//     for (int id1Now = -nQuarkIn; id1Now <= nQuarkIn; ++id1Now)
//     if (id1Now != 0)
//     for (int id2Now = -nQuarkIn; id2Now <= nQuarkIn; ++id2Now)
//     if (id2Now != 0)
//       addPair(id1Now, id2Now);
//   }
// 
//   // Case with q qbar' incoming state.
//   else if (fluxType == "qqbar") {
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//     if (idNow != 0) {
//       addBeamA(idNow);
//       addBeamB(idNow);
//     }
//     for (int id1Now = -nQuarkIn; id1Now <= nQuarkIn; ++id1Now)
//     if (id1Now != 0)
//     for (int id2Now = -nQuarkIn; id2Now <= nQuarkIn; ++id2Now)
//     if (id2Now != 0 && id1Now * id2Now < 0)
//       addPair(id1Now, id2Now);
//   }
// 
//   // Case with q qbar incoming state.
//   else if (fluxType == "qqbarSame") {
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//     if (idNow != 0) {
//       addBeamA(idNow);
//       addBeamB(idNow);
//     }
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//     if (idNow != 0)
//       addPair(idNow, -idNow);
//   }
// 
//   // Case with f f', f fbar', fbar fbar' incoming state.
//   else if (fluxType == "ff") {
//     // If beams are leptons then they are also the colliding partons
//     // unless lepton includes a photon beam.
//     if ( isLeptonA && isLeptonB && !beamA2gamma && !beamB2gamma ) {
//       addBeamA(idA);
//       addBeamB(idB);
//       addPair(idA, idB);
//     // First beam is lepton and second is hadron.
//     } else if ( isLeptonA && !beamA2gamma ) {
//       addBeamA(idA);
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamB(idNow);
//         addPair(idA, idNow);
//       }
//     // First beam is hadron and second is lepton.
//     } else if ( isLeptonB && !beamB2gamma ) {
//       addBeamB(idB);
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addPair(idNow, idB);
//       }
//     // Hadron beams gives quarks.
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addBeamB(idNow);
//       }
//       for (int id1Now = -nQuarkIn; id1Now <= nQuarkIn; ++id1Now)
//       if (id1Now != 0)
//       for (int id2Now = -nQuarkIn; id2Now <= nQuarkIn; ++id2Now)
//       if (id2Now != 0)
//         addPair(id1Now, id2Now);
//     }
//   }
// 
//   // Case with f fbar' generic incoming state.
//   else if (fluxType == "ffbar") {
//     // If beams are leptons then also colliding partons
//     // unless lepton includes a photon beam.
//     if (isLeptonA && isLeptonB && idA * idB < 0
//         && !beamA2gamma && !beamB2gamma) {
//       addBeamA(idA);
//       addBeamB(idB);
//       addPair(idA, idB);
//     // Hadron beams gives quarks.
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addBeamB(idNow);
//       }
//       for (int id1Now = -nQuarkIn; id1Now <= nQuarkIn; ++id1Now)
//       if (id1Now != 0)
//       for (int id2Now = -nQuarkIn; id2Now <= nQuarkIn; ++id2Now)
//       if (id2Now != 0 && id1Now * id2Now < 0)
//         addPair(id1Now, id2Now);
//     }
//   }
// 
//   // Case with f fbar incoming state.
//   else if (fluxType == "ffbarSame") {
//     // If beams are antiparticle pair and leptons then also colliding
//     // partons unless lepton includes a photon beam.
//     if ( idA + idB == 0 && isLeptonA && !beamA2gamma && !beamB2gamma) {
//       addBeamA(idA);
//       addBeamB(idB);
//       addPair(idA, idB);
//     // Else assume both to be hadrons, for better or worse.
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addBeamB(idNow);
//       }
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0)
//         addPair(idNow, -idNow);
//     }
//   }
// 
//   // Case with f fbar' charged(+-1) incoming state.
//   else if (fluxType == "ffbarChg") {
//     // If beams are leptons then also colliding partons
//     // unless lepton includes a photon beam.
//     if ( isLeptonA && isLeptonB && !beamA2gamma && !beamB2gamma
//          && abs( particleDataPtr->chargeType(idA)
//            + particleDataPtr->chargeType(idB) ) == 3 ) {
//       addBeamA(idA);
//       addBeamB(idB);
//       addPair(idA, idB);
//     // Hadron beams gives quarks.
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addBeamB(idNow);
//       }
//       for (int id1Now = -nQuarkIn; id1Now <= nQuarkIn; ++id1Now)
//       if (id1Now != 0)
//       for (int id2Now = -nQuarkIn; id2Now <= nQuarkIn; ++id2Now)
//       if (id2Now != 0 && id1Now * id2Now < 0
//         && (abs(id1Now) + abs(id2Now))%2 == 1) addPair(id1Now, id2Now);
//     }
//   }
// 
//   // Case with f gamma incoming state.
//   else if (fluxType == "fgm") {
//     // Fermion from incoming side A if no photon beam inside.
//     if ( isLeptonA && !beamA2gamma ) {
//       addBeamA( idA);
//       addPair(idA, 22);
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addPair(idNow, 22);
//       }
//     }
//     // Fermion from incoming side B if no photon beam inside.
//     if ( isLeptonB && !beamB2gamma ) {
//       addBeamB( idB);
//       addPair(22, idB);
//     } else {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamB(idNow);
//         addPair(22, idNow);
//       }
//     }
//     // Photons in the beams.
//     addBeamA(22);
//     addBeamB(22);
//   }
// 
//   // Case with quark gamma incoming state.
//   else if (fluxType == "qgm") {
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamA(idNow);
//         addPair(idNow, 22);
//       }
//     // Initialize initiators both ways if not photoproductions.
//     if (!hasGamma) {
//       for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//         if (idNow != 0) {
//           addBeamB(idNow);
//           addPair(22, idNow);
//         }
//     }
//     // Photons in the beams.
//     if (!hasGamma) {
//       addBeamA(22);
//     }
//     addBeamB(22);
//   }
// 
//   // Case with gamma quark incoming state.
//   // Need this when both resolved and unresolved photon beams.
//   else if (fluxType == "gmq") {
//     for (int idNow = -nQuarkIn; idNow <= nQuarkIn; ++idNow)
//       if (idNow != 0) {
//         addBeamB(idNow);
//         addPair(22, idNow);
//       }
//     // Photon in the beam.
//     addBeamA(22);
//   }
// 
//   // Case with gluon gamma incoming state.
//   else if (fluxType == "ggm") {
//     addBeamA(21);
//     addBeamB(22);
//     addPair(21, 22);
// 
//     // If not photoproduction, initialize both ways. Otherwise keep track of
//     // initiator ordering to generate correct combinations (direct,
//     // resolved).
//     if (!hasGamma) {
//       addBeamA(22);
//       addBeamB(21);
//       addPair(22, 21);
//     }
//   }
// 
//   // Case with gamma gluon incoming state.
//   // Need this when both resolved and unresolved photon beams.
//   else if (fluxType == "gmg") {
//     addBeamA(22);
//     addBeamB(21);
//     addPair(22, 21);
//   }
// 
//   // Case with gamma gamma incoming state.
//   else if (fluxType == "gmgm") {
//     addBeamA(22);
//     addBeamB(22);
//     addPair(22, 22);
//   }
// 
//   // Unrecognized fluxType is bad sign. Else done.
//   else {
//     loggerPtr->ERROR_MSG("unrecognized inFlux type", fluxType);
//     return false;
//   }
// 
//   return true;
// 
// }

//--------------------------------------------------------------------------

// Example to Convolute matrix-element expression(s) with parton flux
// and K factor. Possibly different PDFs for the phase-space
// initialization. Can also take new values for x's to correct for
// oversampling, as needed with external photon flux.

// double SigmaProcess::sigmaPDF(bool initPS, bool samexGamma,
//     bool useNewXvalues, double x1New, double x2New) {
// 
//   // Evaluate and store the required parton densities.
//   for (int j = 0; j < sizeBeamA(); ++j) {
//     if ( initPS)
//       inBeamA[j].pdf = beamAPtr->xfMax( inBeamA[j].id, x1Save, Q2FacSave);
//     else if ( samexGamma)
//       inBeamA[j].pdf = beamAPtr->xfSame( inBeamA[j].id, x1Save, Q2FacSave);
//     else if ( useNewXvalues && x1New > 0.)
//       inBeamA[j].pdf = beamAPtr->xfGamma( inBeamA[j].id, x1New, Q2FacSave);
//     else
//       inBeamA[j].pdf = beamAPtr->xfHard( inBeamA[j].id, x1Save, Q2FacSave);
//   }
//   for (int j = 0; j < sizeBeamB(); ++j){
//     if ( initPS)
//       inBeamB[j].pdf = beamBPtr->xfMax( inBeamB[j].id, x2Save, Q2FacSave);
//     else if ( samexGamma)
//       inBeamB[j].pdf = beamBPtr->xfSame( inBeamB[j].id, x2Save, Q2FacSave);
//     else if ( useNewXvalues && x2New > 0.)
//       inBeamB[j].pdf = beamBPtr->xfGamma( inBeamB[j].id, x2New, Q2FacSave);
//     else
//       inBeamB[j].pdf = beamBPtr->xfHard( inBeamB[j].id, x2Save, Q2FacSave);
//   }
// 
//   // Save the x_gamma values after PDFs are called if new value is sampled
//   // if using internal photon flux from leptons.
//   if ( !useNewXvalues && !samexGamma && beamAPtr->hasResGamma() )
//     beamAPtr->xGammaPDF();
//   if ( !useNewXvalues && !samexGamma && beamBPtr->hasResGamma() )
//     beamBPtr->xGammaPDF();
// 
//   // Loop over allowed incoming channels.
//   sigmaSumSave = 0.;
//   for (int i = 0; i < sizePair(); ++i) {
// 
//     // Evaluate hard-scattering cross section. Include K factor.
//     inPair[i].pdfSigma = Kfactor
//                        * sigmaHatWrap(inPair[i].idA, inPair[i].idB);
// 
//     // Multiply by respective parton densities.
//     for (int j = 0; j < sizeBeamA(); ++j)
//     if (inPair[i].idA == inBeamA[j].id) {
//       inPair[i].pdfA      = inBeamA[j].pdf;
//       inPair[i].pdfSigma *= inBeamA[j].pdf;
//       break;
//     }
//     for (int j = 0; j < sizeBeamB(); ++j)
//     if (inPair[i].idB == inBeamB[j].id) {
//       inPair[i].pdfB      = inBeamB[j].pdf;
//       inPair[i].pdfSigma *= inBeamB[j].pdf;
//       break;
//     }
// 
//     // Sum for all channels.
//     sigmaSumSave += inPair[i].pdfSigma;
//   }
// 
//   // Done.
//   return sigmaSumSave;
// 
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
