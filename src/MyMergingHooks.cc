// MyMergingHooks.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyMergingHooks class.

#include "MyPackage/MyMergingHooks.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyMergingHooks class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(MergingHooks, MyMergingHooks,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyMergingHooks::MyMergingHooks(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : MergingHooks() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Example for initialising MergingHooks class.

// void MyMergingHooks::init() {
// 
//   // Save pointers.
//   showers               = 0;
// 
//   // Initialise AlphaS objects for reweighting
//   double alphaSvalueFSR = parm("TimeShower:alphaSvalue");
//   int    alphaSorderFSR = mode("TimeShower:alphaSorder");
//   int    alphaSnfmax    = mode("StandardModel:alphaSnfmax");
//   int    alphaSuseCMWFSR= flag("TimeShower:alphaSuseCMW");
//   AlphaS_FSRSave.init(alphaSvalueFSR, alphaSorderFSR, alphaSnfmax,
//     alphaSuseCMWFSR);
//   double alphaSvalueISR = parm("SpaceShower:alphaSvalue");
//   int    alphaSorderISR = mode("SpaceShower:alphaSorder");
//   int    alphaSuseCMWISR= flag("SpaceShower:alphaSuseCMW");
//   AlphaS_ISRSave.init(alphaSvalueISR, alphaSorderISR, alphaSnfmax,
//     alphaSuseCMWISR);
// 
//   // Initialise AlphaEM objects for reweighting
//   int    alphaEMFSRorder = mode("TimeShower:alphaEMorder");
//   AlphaEM_FSRSave.init(alphaEMFSRorder, settingsPtr);
//   int    alphaEMISRorder = mode("SpaceShower:alphaEMorder");
//   AlphaEM_ISRSave.init(alphaEMISRorder, settingsPtr);
// 
//   // Initialise merging switches
//   doUserMergingSave      = flag("Merging:doUserMerging");
//   // Initialise automated MadGraph kT merging
//   doMGMergingSave        = flag("Merging:doMGMerging");
//   // Initialise kT merging
//   doKTMergingSave        = flag("Merging:doKTMerging");
//   // Initialise evolution-pT merging
//   doPTLundMergingSave    = flag("Merging:doPTLundMerging");
//   // Initialise \Delta_R_{ij}, pT_i Q_{ij} merging
//   doCutBasedMergingSave  = flag("Merging:doCutBasedMerging");
//   // Initialise exact definition of kT
//   ktTypeSave             = mode("Merging:ktType");
// 
//   // Initialise NL3 switches.
//   doNL3TreeSave          = flag("Merging:doNL3Tree");
//   doNL3LoopSave          = flag("Merging:doNL3Loop");
//   doNL3SubtSave          = flag("Merging:doNL3Subt");
//   bool doNL3             = doNL3TreeSave || doNL3LoopSave || doNL3SubtSave;
// 
//   // Initialise UNLOPS switches.
//   doUNLOPSTreeSave      =  flag("Merging:doUNLOPSTree");
//   doUNLOPSLoopSave      =  flag("Merging:doUNLOPSLoop");
//   doUNLOPSSubtSave      =  flag("Merging:doUNLOPSSubt");
//   doUNLOPSSubtNLOSave   =  flag("Merging:doUNLOPSSubtNLO");
//   bool doUNLOPS         = doUNLOPSTreeSave || doUNLOPSLoopSave
//                        || doUNLOPSSubtSave || doUNLOPSSubtNLOSave;
// 
//   // Initialise UMEPS switches
//   doUMEPSTreeSave      =  flag("Merging:doUMEPSTree");
//   doUMEPSSubtSave      =  flag("Merging:doUMEPSSubt");
//   nReclusterSave       =  mode("Merging:nRecluster");
//   nQuarksMergeSave     =  mode("Merging:nQuarksMerge");
//   nRequestedSave       =  mode("Merging:nRequested");
//   bool doUMEPS         =  doUMEPSTreeSave || doUMEPSSubtSave;
// 
//   // Flag to only do phase space cut.
//   doEstimateXSection   =  flag("Merging:doXSectionEstimate");
// 
//   // Flag to check if we have an aMC@NLO runtime interface.
//   doRuntimeAMCATNLOInterfaceSave
//     = settingsPtr->flag("Merging:runtimeAMCATNLOInterface");
// 
//   // Flag to check if merging weight should directly be included in the cross
//   // section.
//   includeWGTinXSECSave = flag("Merging:includeWeightInXsection");
// 
//   // Flag to check if CKKW-L event veto should be applied.
//   applyVeto            =  flag("Merging:applyVeto");
// 
//   // Get core process from user input
//   processSave          = word("Merging:Process");
//   processNow           = processSave;
//   bool doGuess         = (processNow.find("guess") != string::npos);
// 
//   // If the process string is "guess", temporarily set it to something safe
//   // for initialization.
//   if (processNow.find("guess") != string::npos) processNow = "pp>e+e-";
// 
//   if (!hardProcess) {
//     hardProcess = new HardProcess();
//     useOwnHardProcess = true;
//   }
// 
//   // Clear hard process
//   hardProcess->clear();
// 
//   // Initialise input event.
//   inputEvent.init("(hard process)", particleDataPtr);
//   doRemoveDecayProducts = doGuess || flag("Merging:mayRemoveDecayProducts");
//   settingsPtr->flag("Merging:mayRemoveDecayProducts",doRemoveDecayProducts);
// 
//   // Initialise the hard process
//   if ( doMGMergingSave )
//     hardProcess->initOnLHEF(lheInputFile, particleDataPtr);
//   else
//     hardProcess->initOnProcess(processNow, particleDataPtr);
// 
//   // Remove whitespace from process string
//   while(processSave.find(" ", 0) != string::npos)
//     processSave.erase(processSave.begin()+processSave.find(" ",0));
// 
//   // Parameters for reconstruction of evolution scales
//   includeMassiveSave        = flag("Merging:includeMassive");
//   enforceStrongOrderingSave = flag("Merging:enforceStrongOrdering");
//   scaleSeparationFactorSave = parm("Merging:scaleSeparationFactor");
//   orderInRapiditySave       = flag("Merging:orderInRapidity");
// 
//   // Parameters for choosing history probabilistically
//   nonJoinedNormSave     = parm("Merging:nonJoinedNorm");
//   fsrInRecNormSave      = parm("Merging:fsrInRecNorm");
//   pickByFullPSave       = flag("Merging:pickByFullP");
//   pickByPoPT2Save       = flag("Merging:pickByPoPT2");
//   includeRedundantSave  = flag("Merging:includeRedundant");
// 
//   // Parameters for scale choices
//   unorderedScalePrescipSave    = mode("Merging:unorderedScalePrescrip");
//   unorderedASscalePrescipSave  = mode("Merging:unorderedASscalePrescrip");
//   unorderedPDFscalePrescipSave = mode("Merging:unorderedPDFscalePrescrip");
//   incompleteScalePrescipSave   = mode("Merging:incompleteScalePrescrip");
// 
//   // Parameter for allowing swapping of one colour index while reclustering
//   allowColourShufflingSave     = flag("Merging:allowColourShuffling");
// 
//   // Parameters to allow setting hard process scales to default (dynamical)
//   // Pythia values.
//   resetHardQRenSave     =  flag("Merging:usePythiaQRenHard");
//   resetHardQFacSave     =  flag("Merging:usePythiaQFacHard");
// 
//   // Parameters for choosing history by sum(|pT|)
//   pickBySumPTSave       = flag("Merging:pickBySumPT");
//   herwigAcollFSRSave    = parm("Merging:aCollFSR");
//   herwigAcollISRSave    = parm("Merging:aCollISR");
// 
//   // Information on the shower cut-off scale
//   pT0ISRSave            = parm("SpaceShower:pT0Ref");
//   pTminISRSave          = parm("SpaceShower:pTmin");
//   pTminFSRSave          = parm("TimeShower:pTmin");
//   pTcutSave             = max(pTminISRSave,pT0ISRSave);
// 
//   // Information on renormalization scale variations
//   muRVarFactors = infoPtr->weightContainerPtr->weightsMerging.
//     getMuRVarFactors();
//   doVariations = muRVarFactors.size() ? true : false;
//   nWgts = 1+muRVarFactors.size();
// 
//   // Initialise CKKWL weight
//   weightCKKWLSave = vector<double>( nWgts, 1. );
//   weightFIRSTSave = vector<double>( nWgts, 0. );
//   nMinMPISave = 100;
//   muMISave = -1.;
// 
//   // Initialize merging weights in weight container
//   vector<string> weightNames = {"MUR1.0_MUF1.0"};
//   for (double fact: muRVarFactors) {
//     weightNames.push_back("MUR"+std::to_string(fact)+"_MUF1.0");
//   }
//   infoPtr->weightContainerPtr->weightsMerging.bookVectors(
//       weightCKKWLSave,weightFIRSTSave,weightNames);
// 
//   // Initialise merging scale
//   tmsValueSave = 0.;
//   tmsListSave.resize(0);
// 
//   kFactor0jSave         = parm("Merging:kFactor0j");
//   kFactor1jSave         = parm("Merging:kFactor1j");
//   kFactor2jSave         = parm("Merging:kFactor2j");
// 
//   muFSave               = parm("Merging:muFac");
//   muRSave               = parm("Merging:muRen");
//   muFinMESave           = parm("Merging:muFacInME");
//   muRinMESave           = parm("Merging:muRenInME");
// 
//   doWeakClusteringSave  = flag("Merging:allowWeakClustering");
//   doSQCDClusteringSave  = flag("Merging:allowSQCDClustering");
//   DparameterSave        = parm("Merging:Dparameter");
// 
//   // Save merging scale on maximal number of jets
//   if (  doKTMergingSave || doUserMergingSave || doPTLundMergingSave
//     || doUMEPS ) {
//     // Read merging scale (defined in kT) from input parameter.
//     tmsValueSave    = parm("Merging:TMS");
//     nJetMaxSave     = mode("Merging:nJetMax");
//     nJetMaxNLOSave  = -1;
//   } else if (doMGMergingSave) {
//     // Read merging scale (defined in kT) from LHE file.
//     tmsValueSave    = hardProcess->tms;
//     nJetMaxSave     = mode("Merging:nJetMax");
//     nJetMaxNLOSave  = -1;
//   } else if (doCutBasedMergingSave) {
// 
//     // Save list of cuts defining the merging scale.
//     nJetMaxSave     = mode("Merging:nJetMax");
//     nJetMaxNLOSave  = -1;
//     // Write tms cut values to list of cut values,
//     // ordered by DeltaR_{ij}, pT_{i}, Q_{ij}.
//     tmsListSave.resize(0);
//     double drms     = parm("Merging:dRijMS");
//     double ptms     = parm("Merging:pTiMS");
//     double qms      = parm("Merging:QijMS");
//     tmsListSave.push_back(drms);
//     tmsListSave.push_back(ptms);
//     tmsListSave.push_back(qms);
// 
//   }
// 
//   // Read additional settings for NLO merging methods.
//   if ( doNL3 || doUNLOPS || doEstimateXSection ) {
//     tmsValueSave    = parm("Merging:TMS");
//     nJetMaxSave     = mode("Merging:nJetMax");
//     nJetMaxNLOSave  = mode("Merging:nJetMaxNLO");
//   }
// 
//   tmsValueNow = tmsValueSave;
// 
//   // Internal Pythia cross section should not include NLO merging weights.
//   if ( doNL3 || doUNLOPS ) includeWGTinXSECSave = false;
// 
//   hasJetMaxLocal  = false;
//   nJetMaxLocal    = nJetMaxSave;
//   nJetMaxNLOLocal = nJetMaxNLOSave;
// 
//   // Check if external shower plugin should be used.
//   useShowerPluginSave = flag("Merging:useShowerPlugin");
// 
//   bool writeBanner =  doKTMergingSave || doMGMergingSave
//                    || doUserMergingSave
//                    || doNL3 || doUNLOPS || doUMEPS
//                    || doPTLundMergingSave || doCutBasedMergingSave;
// 
// }

//--------------------------------------------------------------------------

// Example function to return the value of the merging scale function
// in the current event.

// double MyMergingHooks::tmsNow( const Event& event ) {
// 
//   // Get merging scale in current event.
//   double tnow = 0.;
//   int unlopsType = mode("Merging:unlopsTMSdefinition");
//   // Use KT/Durham merging scale definition.
//   if ( doKTMerging()  || doMGMerging() )
//     tnow = kTms(event);
//   // Use Lund PT merging scale definition.
//   else if ( doPTLundMerging() )
//     tnow = rhoms(event, false);
//   // Use DeltaR_{ij}, pT_i, Q_{ij} combination merging scale definition.
//   else if ( doCutBasedMerging() )
//     tnow = cutbasedms(event);
//   // Use NLO merging (Lund PT) merging scale definition.
//   else if ( doNL3Merging() )
//     tnow = rhoms(event, false);
//   // Use NLO merging (Lund PT) merging scale definition.
//   else if ( doUNLOPSMerging() )
//     tnow = (unlopsType < 0) ? rhoms(event, false) : tmsDefinition(event);
//   // Use UMEPS (Lund PT) merging scale definition.
//   else if ( doUMEPSMerging() )
//     tnow = rhoms(event, false);
//   // Use user-defined merging scale.
//   else
//     tnow = tmsDefinition(event);
//   // Return merging scale value. Done
//   return tnow;
//   
// }

//--------------------------------------------------------------------------

// Example function to check if emission should be rejected.

// bool MyMergingHooks::doVetoEmission( const Event& event) {
// 
//   // Do nothing in trial showers, or after first step.
//   if ( doIgnoreEmissionsSave ) return false;
// 
//   // Do nothing in CKKW-L
//   if (  doUserMerging() || doMGMerging() || doKTMerging()
//     ||  doPTLundMerging() || doCutBasedMerging() )
//      return false;
// 
//   // For NLO merging, count and veto emissions above the merging scale
//   bool veto = false;
//   // Get number of clustering steps
//   int nSteps  = getNumberOfClusteringSteps(event);
//   // Get merging scale in current event
//   double tnow = tmsNow( event);
// 
//   // Get maximal number of additional jets
//   int nJetMax = nMaxJets();
//   // Always remove emissions above the merging scale for
//   // samples containing reclusterings!
//   if ( nRecluster() > 0 ) nSteps = 1;
//   // Check veto condition
//   if ( nSteps - 1 < nJetMax && nSteps >= 1 && tnow > tms() ) veto = true;
// 
//   // Do not veto if state already includes MPI.
//   if ( infoPtr->nMPI() > 1 ) veto = false;
// 
//   // When performing NL3 merging of tree-level events, reset the
//   // CKKWL weight.
//   if ( veto && doNL3Tree() ) setWeightCKKWL(vector<double>(nWgts, 0.));
// 
//   // If the emission is allowed, do not check any further emissions
//   if ( !veto ) doIgnoreEmissionsSave = true;
// 
//   // Done
//   return veto;
// 
// }

//--------------------------------------------------------------------------

// Example function to set the correct starting scales of the shower.
// Note: 2 -> 2 QCD systems can be produced by MPI. Hence, there is an
// overlap between MPI and "hard" 2 -> 2 QCD systems which needs to be
// removed by no-MPI probabilities. This means that for any "hard" 2 -> 2 QCD
// system, multiparton interactions should start at the maximal scale
// of multiple interactions. The same argument holds for any "hard" process
// that overlaps with MPI.

// bool MyMergingHooks::setShowerStartingScales( bool isTrial,
//   bool doMergeFirstEmm, double& pTscaleIn, const Event& event,
//   double& pTmaxFSRIn, bool& limitPTmaxFSRIn,
//   double& pTmaxISRIn, bool& limitPTmaxISRIn,
//   double& pTmaxMPIIn, bool& limitPTmaxMPIIn ) {
// 
//   // Local copies of power/wimpy shower booleans and scales.
//   bool   limitPTmaxFSR = limitPTmaxFSRIn;
//   bool   limitPTmaxISR = limitPTmaxISRIn;
//   bool   limitPTmaxMPI = limitPTmaxMPIIn;
//   double pTmaxFSR      = pTmaxFSRIn;
//   double pTmaxISR      = pTmaxISRIn;
//   double pTmaxMPI      = pTmaxMPIIn;
//   double pTscale       = pTscaleIn;
// 
//   // Merging of EW+QCD showers with matrix elements: Ensure that
//   // 1. any event with more than one final state particle will be showered
//   //    from the reconstructed transverse momentum of the last emission,
//   //    even if the factorisation scale is low.
//   // 2. the shower starting scale for events with no emission is given by
//   //    the (user-defined) choice.
//   bool isInclusive = ( getProcessString().find("inc") != string::npos );
// 
//   // Check if the process only contains two outgoing partons. If so, then
//   // this process could also have been produced by MPI.
//   // Thus, the MPI starting
//   // scale would need to be set accordingly to correctly attach a
//   // "no-MPI-probability" to multi-jet events. ("Hard" MPI are included
//   // by not restricting MPI when showering the lowest-multiplicity sample.)
//   double pT2to2 = 0;
//   int nFinalPartons = 0, nInitialPartons = 0, nFinalOther = 0;
//   for ( int i = 0; i < event.size(); ++i ) {
//     if ( (event[i].mother1() == 1 || event[i].mother1() == 2 )
//       && (event[i].idAbs()   < 6  || event[i].id()      == 21) )
//       nInitialPartons++;
//     if (event[i].isFinal() && (event[i].idAbs() < 6 || event[i].id() == 21)){
//         nFinalPartons++;
//         pT2to2 = event[i].pT();
//     } else if ( event[i].isFinal() ) nFinalOther++;
//   }
//   bool is2to2QCD     = ( nFinalPartons == 2 && nInitialPartons == 2
//                       && nFinalOther   == 0 );
//   bool hasMPIoverlap = is2to2QCD;
//   bool is2to1        = ( nFinalPartons == 0 );
// 
//   double scale   = event.scale();
// 
//   // SET THE STARTING SCALES FOR TRIAL SHOWERS.
//   if ( isTrial ) {
// 
//     // Reset shower and MPI scales.
//     pTmaxISR = pTmaxFSR = pTmaxMPI = scale;
// 
//     // Reset to minimal scale for wimpy showers. Keep scales for EW+QCD
//     // merging.
//     if ( limitPTmaxISR && !isInclusive ) pTmaxISR = min(scale,muF());
//     if ( limitPTmaxFSR && !isInclusive ) pTmaxFSR = min(scale,muF());
//     if ( limitPTmaxMPI && !isInclusive ) pTmaxMPI = min(scale,muF());
// 
//     // For EW+QCD merging, apply wimpy shower only to 2->1 processes.
//     if (limitPTmaxISR && isInclusive && is2to1 ) pTmaxISR = min(scale,muF());
//     if (limitPTmaxFSR && isInclusive && is2to1 ) pTmaxFSR = min(scale,muF());
//     if (limitPTmaxMPI && isInclusive && is2to1 ) pTmaxMPI = min(scale,muF());
// 
//     // For pure QCD set the PS starting scales to the pT of the dijet system.
//     if (is2to2QCD) {
//       pTmaxFSR = pT2to2;
//       pTmaxISR = pT2to2;
//     }
// 
//     // If necessary, set the MPI starting scale to the collider energy.
//     if ( hasMPIoverlap ) pTmaxMPI = infoPtr->eCM();
// 
//     // Reset phase space limitation flags
//     if ( pTscale < infoPtr->eCM() ) {
//       limitPTmaxISR = limitPTmaxFSR = limitPTmaxMPI = true;
//       // If necessary, set the MPI starting scale to the collider energy.
//       if ( hasMPIoverlap ) limitPTmaxMPI = false;
//     }
// 
//   }
// 
//   // SET THE STARTING SCALES FOR REGULAR SHOWERS.
//   if ( doMergeFirstEmm ) {
// 
//     // Remember if this is a "regular" shower off a reclustered event.
//     bool doRecluster = doUMEPSSubt() || doNL3Subt() || doUNLOPSSubt()
//                     || doUNLOPSSubtNLO();
// 
//     // Reset shower and MPI scales.
//     pTmaxISR = pTmaxFSR = pTmaxMPI = scale;
// 
//     // Reset to minimal scale for wimpy showers. Keep scales for EW+QCD
//     // merging.
//     if ( limitPTmaxISR && !isInclusive ) pTmaxISR = min(scale,muF());
//     if ( limitPTmaxFSR && !isInclusive ) pTmaxFSR = min(scale,muF());
//     if ( limitPTmaxMPI && !isInclusive ) pTmaxMPI = min(scale,muF());
// 
//     // For EW+QCD merging, apply wimpy shower only to 2->1 processes.
//     if (limitPTmaxISR && isInclusive && is2to1 ) pTmaxISR = min(scale,muF());
//     if (limitPTmaxFSR && isInclusive && is2to1 ) pTmaxFSR = min(scale,muF());
//     if (limitPTmaxMPI && isInclusive && is2to1 ) pTmaxMPI = min(scale,muF());
// 
//     // For pure QCD set the PS starting scales to the pT of the dijet system.
//     if (is2to2QCD) {
//       pTmaxFSR = pT2to2;
//       pTmaxISR = pT2to2;
//     }
// 
//     // If necessary, set the MPI starting scale to the collider energy.
//     if ( hasMPIoverlap && !doRecluster ) {
//       pTmaxMPI = infoPtr->eCM();
//       limitPTmaxMPI = false;
//     }
// 
//     // For reclustered events, no-MPI-probability between "pTmaxMPI" and
//     // "scale" already included in the event weight.
//     if ( doRecluster ) {
//       pTmaxMPI      = muMI();
//       limitPTmaxMPI = true;
//     }
//   }
// 
//   // Reset power/wimpy shower switches iand scales if necessary.
//   limitPTmaxFSRIn = limitPTmaxFSR;
//   limitPTmaxISRIn = limitPTmaxISR;
//   limitPTmaxMPIIn = limitPTmaxMPI;
//   pTmaxFSRIn      = pTmaxFSR;
//   pTmaxISRIn      = pTmaxISR;
//   pTmaxMPIIn      = pTmaxMPI;
//   pTscaleIn       = pTscale;
// 
//   // Done
//   return true;
// 
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
