// MyDecayHandler.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyDecayHandler class.

#include "MyPackage/MyDecayHandler.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyDecayHandler class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(DecayHandler, MyDecayHandler,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyDecayHandler::MyDecayHandler(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : DecayHandler() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Perform a single decay.

// bool MyDecayHandler::decay(vector<int>& idProd, vector<double>& mProd,
//   vector<Vec4>& pProd, int iDec, const Event& event) {
// 
//   // idProd: is a list of particle PDG identity codes,
//   // mProd:  is a list of their respective masses (in GeV), and
//   // pProd: is a list of their respective four-momenta.
// 
//   // At input, these vectors each have size one, so that idProd[0],
//   // mProd[0] and pProd[0] contain information on the particle that is
//   // to be decayed. At output, the vectors should have increased by
//   // the addition of all the decay products. Even if initially defined
//   // in the rest frame of the mother, the products should have been
//   // boosted so that their four-momenta add up to the pProd[0] of the
//   // decaying particle.
// 
//   // Should it be of interest to know the prehistory of the decaying
//   // particle, e.g. to set some helicity information affecting the
//   // decay angular distribution, the full event record is available
//   // read-only, with info in which slot iDec the decaying particle is
//   // stored.
// 
//   // The routine should return true if it managed the decay and false
//   // otherwise, in which case Pythia will try to do the decay
//   // itself. This e.g. means you can choose to do some decay channels
//   // yourself, and leave others to Pythia. To avoid double-counting,
//   // the channels you want to handle should be switched off in the
//   // Pythia particle database. In the beginning of the external decay
//   // method you should then return false with a probability given by
//   // the sum of the branching ratios for those channels you do not
//   // want to handle yourself.
// 
//   // Note that the decay vertex is always set by Pythia, and that
//   // B-Bbar oscillations have already been taken into account, if they
//   // were switched on. Thus idProd[0] may be the opposite of
//   // event[iDec].id(), where the latter provides the code at
//   // production.
//   
//   // Return if the decay was performed.
//   return false;
// 
// }

//--------------------------------------------------------------------------

// Peform a full decay chain.

// bool MyDecayHandler::chainDecay(vector<int>& idProd, vector<int>& motherProd,
//   vector<double>& mProd, vector<Vec4>& pProd, int iDec,
//   const Event& event) {
// 
//   // Here the new motherProd vector also has size one at input, with
//   // motherProd[0] = 0. At output it should have increaed in size in
//   // the same way as the other arrays. Particles that come directly
//   // from the mother should have value 0, whereas secondary decay
//   // products should have the index of the mother in the arrays. To
//   // simplify parsing, particles having the same mother should be
//   // placed consecutively in the arrays, and daughters can not be put
//   // before their mothers. When the particles are transferred to the
//   // standard event record, the full mother-daughter relations will be
//   // reconstructed from the new array, and any particle with daughters
//   // will be considered to have decayed. For long-lived intermediate
//   // particles also vertex information will take this into
//   // account. User-selected secondary decay channels will be accepted
//   // as they are, however, without any knowledge whether the user has
//   // allowed for particle-antiparticle oscillations before that
//   // decay. Therefore a simple exponential decay time will be used to
//   // find secondary vertices.
// 
//   // While primarily intended for sequential decays, of course the
//   // chainDecay method can be used also for simple decays in one step,
//   // and is then equivalent with decay one. This is useful if a
//   // particle species has some decay channels that lead to sequential
//   // decays whereas others do not. During code execution it is first
//   // checked whether chainDecay can do the decay, and if not decay is
//   // offered to. By default chainDecay returns false, so if you only
//   // overload decay it will be called. If you want to you can choose
//   // to handle the decays of some particles in one of the methods and
//   // other particles in the other method, so long as you return false
//   // for those decays you do not handle.
//   
//   // Return if the decay was performed.
//   return false.
//   
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
