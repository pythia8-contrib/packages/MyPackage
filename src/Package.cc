// Package.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Required file where global package definitions are given, including
// which versions of Pythia the package is compatible with and
// possible setting specifications.

#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Explicitly define the versions of Pythia that this package is
// compatible with. This macro call is required, and an arbitrary
// number of versions can be passed.

PYTHIA8_PLUGIN_VERSIONS(8310, 8311)

//==========================================================================

// Optionally define a path to XML documentation included with this
// package. This macro should be called once for a given plugin
// library. If there is no XML documentation for this plugin library,
// then remove this macro call.

PYTHIA8_PLUGIN_XML("MyPackage/xmldoc/Index.xml") 

//==========================================================================

// The following optional example code registers settings for the
// plugin library. Note, the macro PYTHIA8_PLUGIN_SETTINGS should only
// be called once for a given plugin library; the settings for all
// plugin classes in this plugin library should be defined here. If no
// settings need to be defined, then this code (method and macro
// below) can be removed from the library.

//--------------------------------------------------------------------------

// Example method to register settings.

void MyPackageSettings(Settings* settingsPtr) {

  // The following "add" methods for the "Settings" class allow
  // settings to be added to the settings database. Possible setting
  // types are boolean (flag), integer (mode), double (parm), string
  // (word), vector of booleans (fvec), vector of integers (mvec),
  // vector of doubles (pvec), and vector of string (wvec). For
  // clarity, the argument types are included in the comments below,
  // but should of course be removed when arguments to the relevant
  // methods. It is recommended that all settings begin with the
  // MyPackage prefix, to prevent collisions with settings from other
  // PYTHIA-CONTRIB packages, as well as Pythia itself. The arguments
  // are as follows:
  
  //
  // keyIn:     a string that is the setting name, i.e. when reading a
  //            command string such as "MySetting = Value", "keyIn" is
  //            "MySetting".
  // defaultIn: default value for the setting, must be of the same type
  //            as "add" method called, e.g. the "defaultIn" for "addMode"
  //            is an integer.
  // hasMinIn:  boolean flag if there is minimum allowed value for the
  //            setting.
  // hasMaxIn:  same as "hasMinIn", but for maximum.
  // minIn:     specifies the optional minimum, of the same type as the
  //            method, e.g. a double for "addParm".
  // maxIn:     same as "minIn", but for maximum.
  // optOnlyIn: a boolean which specifies only allowed options are
  //            considered valid, for the "addMode" method only.

  // // Add a boolean setting (flag).
  // settingsPtr->addFlag(string keyIn, bool defaultIn);
  
  // // Add an integer setting (mode).
  // settingsPtr->addMode(string keyIn, int defaultIn, bool hasMinIn,
  //   bool hasMaxIn, int minIn, int maxIn, bool optOnlyIn = false);

  // // Add a double setting (parm).
  // settingsPtr->addParm(string keyIn, double defaultIn, bool hasMinIn,
  //   bool hasMaxIn, double minIn, double maxIn);

  // // Add a string setting (word).
  // settingsPtr->addWord(string keyIn, string defaultIn);

  // // Add a vector of booleans setting (fvec).
  // settingsPtr->addFVec(string keyIn, vector<bool> defaultIn);

  // // Add a vector of integers setting (mvec).
  // settingsPtr->addMVec(string keyIn, vector<int> defaultIn, bool hasMinIn,
  //  bool hasMaxIn, int minIn, int maxIn);

  // // Add a vector of doubles setting (pvec).
  // settingsPtr->addPVec(string keyIn, vector<double> defaultIn, bool hasMinIn,
  //   bool hasMaxIn, double minIn, double maxIn);

  // // Add a vector of strings setting (wvec).
  // settingsPtr->addWVec(string keyIn, vector<string> defaultIn);

  // Here, we define a single setting as an example.
  settingsPtr->addFlag("MyPackage:registered", true);
  
}

//--------------------------------------------------------------------------

// Register the setting registration method above so that it can be
// loaded by Pythia. This macro should only be called once per plugin
// library.

PYTHIA8_PLUGIN_SETTINGS(MyPackageSettings)

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
