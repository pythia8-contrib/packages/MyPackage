// MyPhaseSpace.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyPhaseSpace class.

#include "MyPackage/MyPhaseSpace.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyPhaseSpace class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(PhaseSpace, MyPhaseSpace,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyPhaseSpace::MyPhaseSpace(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : PhaseSpace() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Any more involved methods should be defined here for the
// MyPhaseSpace class.

// Some details are given below on PhaseSpace methods which must be
// defined.

// In the setupSampling() step the main point is to determine the
// upper estimate of the cross section integrated over the allowed
// phase space regions, and this should be stored in sigmaMx. The
// ratio between the correct cross section and its upper estimate is a
// measure of the phase-space selection efficiency, and the purpose of
// this step is to optimize the sampling accordingly. To this end any
// convenient set of phase-space variables may be chosen. The x1H and
// x2H varables should be used to denote the incoming parton momentum
// fractions, however, to be used in PDF evaluations.

// In the trialKin() intermediate step the same set of internal
// variables can be used, and fed into the SigmaProcess code to
// evaluate the cross section in the given phase space point,
// multiplied by the integrated cross section. This value is to be
// stored in sigmaNw, and the ratio sigmaNw/sigmaMx will be used to
// determine whether the trial event is accepted or not.

// In the finalKin() step the output is more standardized. The key
// values are the ones stored in the mH[] and pH[] arrays, the former
// for masses and the latter for four-momenta. Here the first two
// slots represent the two incoming partons and the subsequent ones up
// to ten outgoing particles. Other particle properties, like the
// number of final-state particles, their identities and colours, and
// more, are defined by the SigmaProcess class.

// A tailor-made 2 -> 3 generator could be defined, e.g., by starting
// from the code for PYTHIA's internal PhaseSpace2to3tauycyl base
// class, which provides a specific representation of 3-parton phase
// space, used for generic 2 -> 3 processes in PYTHIA. The virtual
// functions described could then be redefined to generate a different
// sampling of 3-parton phase space. One example of this is provided
// by the existing PhaseSpace2to3yyycyl class, which PYTHIA uses for
// massless QCD processes. Note the interplay between the phase-space
// variables, generated and saved here, and how they are used by the
// matrix-element codes. For general processes, the user can define
// samplings in terms of their own phase-space parametrizations, as
// long as the corresponding matrix elements use the same variables to
// evaluate the cross-section expressions.

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
