// MyUserHooks.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyUserHooks class.

#include "MyPackage/MyUserHooks.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyUserHooks class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(UserHooks, MyUserHooks,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyUserHooks::MyUserHooks(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : UserHooks() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// multiplySigmaBy allows the user to introduce a multiplicative factor
// that modifies the cross section of a hard process. Since it is called
// from before the event record is generated in full, the normal analysis
// does not work. The code here provides a rather extensive summary of
// which methods actually do work. It is a convenient starting point for
// writing your own derived routine.

// double MyUserHooks::multiplySigmaBy(const SigmaProcess* sigmaProcessPtr,
//   const PhaseSpace* phaseSpacePtr, bool inEvent) {
// 
//   // Process code, necessary when some to be treated differently.
//   int code       = sigmaProcessPtr->code();
// 
//   // Final multiplicity, i.e. whether 2 -> 1 or 2 -> 2.
//   int nFinal     = sigmaProcessPtr->nFinal();
// 
//   // Incoming x1 and x2 to the hard collision, and factorization scale.
//   double x1      = phaseSpacePtr->x1();
//   double x2      = phaseSpacePtr->x2();
//   double Q2Fac   = sigmaProcessPtr->Q2Fac();
// 
//   // Renormalization scale and assumed alpha_strong and alpha_EM.
//   double Q2Ren   = sigmaProcessPtr->Q2Ren();
//   double alphaS  = sigmaProcessPtr->alphaSRen();
//   double alphaEM = sigmaProcessPtr->alphaEMRen();
// 
//   // Subprocess mass-square.
//   double sHat = phaseSpacePtr->sHat();
// 
//   // Now methods only relevant for 2 -> 2.
//   if (nFinal == 2) {
// 
//     // Mandelstam variables and hard-process pT.
//     double tHat  = phaseSpacePtr->tHat();
//     double uHat  = phaseSpacePtr->uHat();
//     double pTHat = phaseSpacePtr->pTHat();
// 
//     // Masses of the final-state particles. (Here 0 for light quarks.)
//     double m3    = sigmaProcessPtr->m(3);
//     double m4    = sigmaProcessPtr->m(4);
//   }
// 
//   // Dummy statement.
//   return 1.;
// 
// }

//--------------------------------------------------------------------------

// biasSelectionBy allows the user to introduce a multiplicative factor
// that modifies the cross section of a hard process. The event is assigned
// a wegith that is the inverse of the selection bias, such that the
// cross section is unchanged. Since it is called from before the
// event record is generated in full, the normal analysis does not work.
// The code here provides a rather extensive summary of which methods
// actually do work. It is a convenient starting point for writing
// your own derived routine.

// double MyUserHooks::biasSelectionBy(const SigmaProcess* sigmaProcessPtr,
//   const PhaseSpace* phaseSpacePtr, bool inEvent) {
// 
//   // Process code, necessary when some to be treated differently.
//   int code       = sigmaProcessPtr->code();
// 
//   // Final multiplicity, i.e. whether 2 -> 1 or 2 -> 2.
//   int nFinal     = sigmaProcessPtr->nFinal();
// 
//   // Incoming x1 and x2 to the hard collision, and factorization scale.
//   double x1      = phaseSpacePtr->x1();
//   double x2      = phaseSpacePtr->x2();
//   double Q2Fac   = sigmaProcessPtr->Q2Fac();
// 
//   // Renormalization scale and assumed alpha_strong and alpha_EM.
//   double Q2Ren   = sigmaProcessPtr->Q2Ren();
//   double alphaS  = sigmaProcessPtr->alphaSRen();
//   double alphaEM = sigmaProcessPtr->alphaEMRen();
// 
//   // Subprocess mass-square.
//   double sHat = phaseSpacePtr->sHat();
// 
//   // Now methods only relevant for 2 -> 2.
//   if (nFinal == 2) {
// 
//     // Mandelstam variables and hard-process pT.
//     double tHat  = phaseSpacePtr->tHat();
//     double uHat  = phaseSpacePtr->uHat();
//     double pTHat = phaseSpacePtr->pTHat();
// 
//     // Masses of the final-state particles. (Here 0 for light quarks.)
//     double m3    = sigmaProcessPtr->m(3);
//     double m4    = sigmaProcessPtr->m(4);
//   }
// 
//   // Insert here your calculation of the selection bias.
//   // Here illustrated by a weighting up of events at high pT.
//   selBias = pow4(phaseSpacePtr->pTHat());
// 
//   // Return the selBias weight.
//   // Warning: if you use another variable than selBias
//   // the compensating weight will not be set correctly.
//   return selBias;
// 
//   // Dummy statement.
//   return 1.;
// 
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
