// MyMerging.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the MyMerging class.

#include "MyPackage/MyMerging.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// The MyMerging class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(Merging, MyMerging,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

MyMerging::MyMerging(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : Merging() {

  // If a pointer is not needed its name (but not type) can be
  // commented out above, e.g. change pythiaPtrIn to /*pythiaPtrIn*/,
  // to prevent compiler warnings. If a pointer is not needed ensure
  // to specify is not required in the corresponding call to the macro
  // PYTHIA8_PLUGIN_CLASS above.
  
  // Include whatever code is needed here to construct the plugin
  // class. Below, we give some examples of how the pointer arguments
  // can be used.

  // Check the Pythia version is consistent with its XML settings
  // (this is already done in the Pythia constructor).
  if (pythiaPtrIn->checkVersion())
    loggerPtrIn->INFO_MSG("the Pythia code and XML versions match");
  
  // Access the flag "MyPackage:registered" as defined by the method in
  // MyPackageSettings of Package.cc.
  loggerPtrIn->INFO_MSG("library setting MyPackage:registered is " + to_string(
      settingsPtrIn->flag("MyPackage:registered")));

  // Access and print the settings as defined by the XML files.
  loggerPtrIn->INFO_MSG("XML setting MyPackage:flag is " + to_string(
      settingsPtrIn->flag("MyPackage:flag")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modeopen is " + to_string(
      settingsPtrIn->mode("MyPackage:modeopen")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:modepick is " + to_string(
      settingsPtrIn->mode("MyPackage:modepick")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:parm is " + to_string(
      settingsPtrIn->parm("MyPackage:parm")));
  loggerPtrIn->INFO_MSG("XML setting MyPackage:word is " + 
      settingsPtrIn->word("MyPackage:word"));
  
  // Access and print the vector settings as defined by the XML files.
  vector<bool> fvec = settingsPtrIn->fvec("MyPackage:fvec");
  string fstr = "{";
  for (const bool &val : fvec) fstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:fvec is " +
    fstr.substr(0, fstr.size() - 1) + "}");
  vector<int> mvec = settingsPtrIn->mvec("MyPackage:mvec");
  string mstr = "{";
  for (const int &val : mvec) mstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:mvec is " +
    mstr.substr(0, mstr.size() - 1) + "}");
  vector<double> pvec = settingsPtrIn->pvec("MyPackage:pvec");
  string pstr = "{";
  for (const double &val : pvec) pstr += to_string(val) + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:pvec is " +
    pstr.substr(0, pstr.size() - 1) + "}");
  vector<string> wvec = settingsPtrIn->wvec("MyPackage:wvec");
  string wstr = "{";
  for (const string &val : wvec) wstr += val + ",";
  loggerPtrIn->INFO_MSG("XML setting MyPackage:wvec is " +
    wstr.substr(0, wstr.size() - 1) + "}");
  
}

//--------------------------------------------------------------------------

// Example function to print information.

// void MyMerging::statistics() {
// 
//   // Recall switch to enfore merging scale cut.
//   bool enforceCutOnLHE  = flag("Merging:enforceCutOnLHE");
//   // Recall merging scale value.
//   double tmsval         = mergingHooksPtr ? mergingHooksPtr->tms() : 0;
//   bool printBanner      = enforceCutOnLHE && tmsNowMin > TMSMISMATCH*tmsval;
//   // Reset minimal tms value.
//   tmsNowMin             = infoPtr->eCM();
// 
//   if (!printBanner) return;
// 
//   // Header.
//   cout << "\n *-------  PYTHIA Matrix Element Merging Information  ------"
//        << "-------------------------------------------------------*\n"
//        << " |                                                            "
//        << "                                                     |\n";
//   // Print warning if the minimal tms value of any event was significantly
//   // above the desired merging scale value.
//   cout << " | Warning in MyMerging::statistics: All Les Houches events"
//        << " significantly above Merging:TMS cut. Please check.       |\n";
// 
//   // Listing finished.
//   cout << " |                                                            "
//        << "                                                     |\n"
//        << " *-------  End PYTHIA Matrix Element Merging Information -----"
//        << "-----------------------------------------------------*" << endl;
// }

//--------------------------------------------------------------------------

// Example function to steer different merging prescriptions.

// int MyMerging::mergeProcess(Event& process){
// 
//   int vetoCode = 1;
// 
//   // Reinitialise hard process.
//   mergingHooksPtr->hardProcess->clear();
//   mergingHooksPtr->processNow = word("Merging:Process");
//   mergingHooksPtr->hardProcess->initOnProcess(
//     mergingHooksPtr->processNow, particleDataPtr);
// 
//   // Determine the merging process.
//   settingsPtr->word("Merging:Process", mergingHooksPtr->processSave);
// 
//   // Determine the merging scheme.
//   mergingHooksPtr->doUserMergingSave = flag("Merging:doUserMerging");
//   mergingHooksPtr->doMGMergingSave = flag("Merging:doMGMerging");
//   mergingHooksPtr->doKTMergingSave = flag("Merging:doKTMerging");
//   mergingHooksPtr->doPTLundMergingSave = flag("Merging:doPTLundMerging");
//   mergingHooksPtr->doCutBasedMergingSave = flag("Merging:doCutBasedMerging");
//   mergingHooksPtr->doNL3TreeSave = flag("Merging:doNL3Tree");
//   mergingHooksPtr->doNL3LoopSave = flag("Merging:doNL3Loop");
//   mergingHooksPtr->doNL3SubtSave = flag("Merging:doNL3Subt");
//   mergingHooksPtr->doUNLOPSTreeSave = flag("Merging:doUNLOPSTree");
//   mergingHooksPtr->doUNLOPSLoopSave = flag("Merging:doUNLOPSLoop");
//   mergingHooksPtr->doUNLOPSSubtSave = flag("Merging:doUNLOPSSubt");
//   mergingHooksPtr->doUNLOPSSubtNLOSave = flag("Merging:doUNLOPSSubtNLO");
//   mergingHooksPtr->doUMEPSTreeSave = flag("Merging:doUMEPSTree");
//   mergingHooksPtr->doUMEPSSubtSave = flag("Merging:doUMEPSSubt");
//   mergingHooksPtr->nReclusterSave = mode("Merging:nRecluster");
// 
//   // Determine jet properties.
//   mergingHooksPtr->hasJetMaxLocal  = false;
//   mergingHooksPtr->nJetMaxLocal = mergingHooksPtr->nJetMaxSave;
//   mergingHooksPtr->nJetMaxNLOLocal = mergingHooksPtr->nJetMaxNLOSave;
//   mergingHooksPtr->nRequestedSave = mode("Merging:nRequested");
// 
//   // Ensure that merging weight is not counted twice.
//   bool includeWGT = mergingHooksPtr->includeWGTinXSEC();
// 
//   // Possibility to apply merging scale to an input event.
//   bool applyTMSCut = flag("Merging:doXSectionEstimate");
//   if ( applyTMSCut && cutOnProcess(process) ) {
//     if (includeWGT) infoPtr->weightContainerPtr->setWeightNominal(0.);
//     return -1;
//   }
//   // Done if only a cut should be applied.
//   if ( applyTMSCut ) return 1;
// 
//   // For the runtime interface between aMCatNLO and Pythia, simply
//   // reconstruct scale and dead zone information and exit.
//   if (mergingHooksPtr->doRuntimeAMCATNLOInterface())
//     return clusterAndStore(process);
// 
//   // Possibility to perform CKKW-L merging on this event.
//   if ( mergingHooksPtr->doCKKWLMerging() )
//     vetoCode = mergeProcessCKKWL(process);
// 
//   // Possibility to perform UMEPS merging on this event.
//   if ( mergingHooksPtr->doUMEPSMerging() )
//      vetoCode = mergeProcessUMEPS(process);
// 
//   // Possibility to perform NL3 NLO merging on this event.
//   if ( mergingHooksPtr->doNL3Merging() )
//     vetoCode = mergeProcessNL3(process);
// 
//   // Possibility to perform UNLOPS merging on this event.
//   if ( mergingHooksPtr->doUNLOPSMerging() )
//     vetoCode = mergeProcessUNLOPS(process);
// 
//   return vetoCode;
// 
// }

//--------------------------------------------------------------------------

// Example function to retrieve shower scale information (to be used
// to set scales in aMCatNLO-LHEF-production. This function is not
// used internally, only when interfacing to aMCatNLO at runtime.

// void MyMerging::getStoppingInfo(double scales[100][100],
//   double masses[100][100]) {
// 
//   int posOffest = 2;
//   for (unsigned int i = 0; i < radSave.size(); ++i){
//     // In fortran we want scales(radiator,recoiler), hence, we should
//     // take the transpose here, since 2-d arrays are treated
//     // differently in c++ and fortran.
//     scales[recSave[i]-posOffest][radSave[i]-posOffest] =
//       stoppingScalesSave[i];
//     masses[recSave[i]-posOffest][radSave[i]-posOffest] = mDipSave[i];
//   }
// 
// }

//--------------------------------------------------------------------------

// Example function to retrieve if any of the shower scales would not
// have been possible to produce by Pythia. This function is not used
// internally, only when interfacing to aMCatNLO at runtime.

// void MyMerging::getDeadzones(bool dzone[100][100]) {
//   int posOffest = 2;
//   for (unsigned int i = 0; i < radSave.size(); ++i){
//     // In fortran we want dzone(radiator,recoiler), hence, we should
//     // take the transpose here, since 2-d arrays are treated
//     // differently in c++ and fortran.
//     dzone[recSave[i]-posOffest][radSave[i]-posOffest] = isInDeadzone[i];
//   }
// }

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Contrib

} // end namespace Pythia8
