# Documentation

Documentation for the package should be provided in `README.md` as a markdown file. Additional documentation can be provided by the authors using the mechanism of their choice, but at a minimum, `README.md` is required. The contents of this file are parsed when building the index of all PYTHIA-CONTRIB packages, and are then displayed in the PYTHIA-CONTRIB summary pages.

The following documentation is required to be provided, and should adhere to the same syntax as given here, namely the opening and closing tags for each requested type of documentation. Note, the tags do not need to be included on their own line; this is done below for clarity. Additionally, this documentation can be included in within a general comment tag, *e.g.* `<!--` and `-->`, if you do not want the required documentation to display in the rendering of `README.md`. Throughout, markdown syntax may be used.

## Package Description 

Everything included within the text bracketed by the tags `<!--description-->` and `<!--/description-->` will be interpreted as the description of the package. This should provide a brief summary of the package and its use. More details can be given elsewhere in the `README.md`. The following provides an example.

<!--description-->
This is the example description for the template building tool for PYTHIA-CONTRIB. The tool can be used to build template packages that can be used as part of the Pythia plugin system.
<!--/description-->

## Package Authors

The authors for the package should be enclosed within the `<!--authors-->` and `<!--/authors-->` tags. General markdown may be used in this description, for example to specify email addresses, *etc.* The author list is not required to follow a specific format, but it is suggested to use the format `last name, first name <email address>; ...`. An example is given below.

<!--authors-->
MLhad team; Ilten, Philip [<philten\@cern.ch>](mailto:philten@cern.ch); Szewc, Manuel [<szewcml\@ucmail.uc.edu>](mailto:szewcml@ucmail.uc.edu)
<!--/authors-->

## Package Dependencies

Any dependencies for the package should be listed in `README.md`, enclosed in the `<!--deps-->` and `<!--/deps-->` tags. This is not a technical listing, *i.e.* it is not used when configuring the package, but rather is intended for users to quickly see what the dependencies are.

<!--deps-->
ROOT ([root.cern](https://root.cern/), > 6.28/10); FASTJET ([fastjet.fr](https://fastjet.fr/), > 3.4.2)
<!--/deps-->

## Package Documentation

Any relevant documentation should be provided in `README.md` within the tags `<!--docs-->` and `<!--/docs-->`. By default, a Doxygen page will be generated for every package in PYTHIA-CONTRIB, as well as the HTML pages, if XML pages have been provided. This can, for example, include papers or websites. An example is given below.

<!--docs-->
The MLhad project is documented via a number of websites, codebases, and papers, listed below.
* [MLhad website](https://uchep.gitlab.io/mlhad-docs/): provides a comprehensive description of the MLhad project.
* [Towards a data-driven model of hadronization using normalizing flows](https://inspirehep.net/literature/2723190): paper describing a normalizing flow hadronization model, and methods to train this model with data.
<!--/docs-->

# Optional Configuration

When using the provided build system, the following (only visible in the source of `README.md`, not in the rendered markdown) can be used to configure external packages. Note, this configuration must be present in `README.md`, but can be hidden with comment tags (as is done here), or included in the main text of the `README.md`. It is possible to define multiple external packages in `README.md`. Whenever the next `PKG=` line is encountered, a new external package is defined. If options for a package are missing, then they are taken as the default value. Note, each configuration key must begin the line and follow the syntax `KEY=VALUE`. The following keys are allowed.
* `PKG`: name of the external package to configure (case does not matter). The configuration script will then use this name for configuring the package. For example, `MyPackage=LHAPDF6` will then include the options `--with-lhapdf6`.
* `CFG`: optional configure script for the package, for example `fastjet-config`.
* `BIN_PATH`: given the root package directory, relative path to the binary directory, or absolute path. By default, this is the relative path `bin`. Alternatively, this can be an argument given to the configure script specified by `CFG`. For example, this could be set to `--bindir` if the configure script accepts this as a valid argument. If the configure script is not available, or if this argument is invalid, the default value of `bin` will be used.
* `INC_PATH`: same as for `BIN_PATH`, but for the header directory; by default, this is `include`.
* `LIB_PATH`: same as above but for the library directory; by default, this is `lib`.
* `BIN_DEPS`: requred executables along `BIN_PATH`, given as a semi-colon separated list. For example, `rootcint;root-config` requires the executables `rootcint` and `root-config` exist in `BIN_PATH`.
* `INC_DEPS`: same as above for `BIN_DEPS`, except now required headers along `INC_PATH`.
* `LIB_DEPS`: same as above for `BIN_DEPS`, except now required libraries along `LIB_PATH`.
* `PKG_DEPS`: it is possible to require additional external packages as dependencies for this external package. This is typically needed when there is no config script for the external package. The list should be provided as a semi-colon separated list, for example `LHAPDF6;FASTJET`. These packages then should also be defined in this configuration.
* `FLAGS`: optionally specify any additional compiler flags that should be used when this package is enabled. For example preprocessor directives can be passed with the option `-D<argument>`.
* `REQUIRE`: if set to `true`, *e.g.* `REQUIRE=true`, this dependency is required, and the configuration will fail unless the package is configured; by default this is `false`.

<!--
Some common dependencies are defined here, which should be modified as necessary, or removed if not used. The general format is as follows (empty packages are not added).

PKG=
CFG=
BIN_PATH=
INC_PATH=
LIB_PATH=
BIN_DEPS=
INC_DEPS=
LIB_DEPS=
PKG_DEPS=
FLAGS=
REQUIRE=

At a minimum, Pythia should be required.

PKG=PYTHIA8
CFG=pythia8-config
INC_PATH=--cxxflags
LIB_PATH=--libs
INC_DEPS=Pythia8/Plugins.h
REQUIRE=true

PKG=EVTGEN
INC_DEPS=EvtGen/EvtGen.hh
LIB_DEPS=EvtGen;EvtGenExternal
PKG_DEPS=HEPMC2

PKG=FASTJET3
CFG=fastjet-config
INC_PATH=--cxxflags
LIB_PATH=--libs
INC_DEPS=fastjet/config.h
LIB_DEPS=fastjet

PKG=HEPMC2
INC_DEPS=HepMC/GenEvent.h
LIB_DEPS=HepMC

PKG=HEPMC3
CFG=HepMC3-config
INC_PATH=--includedir
LIB_PATH=--libdir
INC_DEPS=HepMC3/GenEvent.h
LIB_DEPS=HepMC3

PKG=LHAPDF5
CFG=lhapdf-config
INC_PATH=--incdir
LIB_PATH=--libdir
LIB_DEPS=LHAPDF

PKG=LHAPDF6
CFG=lhapdf-config
INC_PATH=--incdir
LIB_PATH=--libdir
INC_DEPS=LHAPDF/LHAPDF.h
LIB_DEPS=LHAPDF

PKG=RIVET
CFG=rivet-config
INC_PATH=--includedir
LIB_PATH=--libdir
BIN_DEPS=rivet-config
INC_DEPS=Rivet/Rivet.hh
LIB_DEPS=Rivet

PKG=ROOT
CFG=root-config
BIN_PATH=--bindir
INC_PATH=--incdir
LIB_PATH=--libdir
BIN_DEPS=rootcint;root-config
INC_DEPS=TROOT.h
LIB_DEPS=Core

PKG=GZIP
INC_DEPS=zlib.h
LIB_DEPS=z
FLAGS=-DGZIP

PKG=PYTHON
CFG=python-config
INC_PATH=--includes
INC_DEPS=Python.h

PKG=OPENMP
LIB_DEPS=gomp
FLAGS=-fopenmp -DOPENMP

PKG=MPICH
BIN_DEPS=mpic++
INC_DEPS=mpi.h
LIB_DEPS=mpi

PKG=HDF5
INC_DEPS=hdf5.h
LIB_DEPS=hdf5

PKG=HIGHFIVE
INC_DEPS=highfive/H5File.hpp
-->
