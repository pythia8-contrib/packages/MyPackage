// MyRndmEngine.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyRndmEngine_H
#define MyPackage_MyRndmEngine_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyRndmEngine here.
// The following template declares all methods that can be inherited from
// RndmEngine.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyRndmEngine.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyRndmEngine.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyRndmEngine : public RndmEngine {

public:

  // Constructor (defined in src/MyRndmEngine.cc).
  MyRndmEngine(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyRndmEngine() = default;

  // // A virtual method, wherein the derived class method
  // // generates a random number uniformly distributed between 0 and 1.
  // double flat() override {return 1;}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyRndmEngine_H
