// MyMerging.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyMerging_H
#define MyPackage_MyMerging_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyMerging here.
// The following template declares all methods that can be inherited from
// Merging.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyMerging.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyMerging.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyMerging : public Merging {

public:

  // Constructor (defined in src/MyMerging.cc).
  MyMerging(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyMerging() = default;

  // // Initialisation function for internal use inside Pythia source code
  // void initPtrs( MergingHooksPtr mergingHooksPtrIn,
  //   PartonLevel* trialPartonLevelPtrIn) override {
  //   trialPartonLevelPtr = trialPartonLevelPtrIn;
  //   mergingHooksPtr = mergingHooksPtrIn;
  // }

  // // Initialisation function for internal use inside Pythia source code
  // void init() override;
  // {tmsNowMin = infoPtr->eCM();}

  // // Function to print statistics.
  // void statistics() override;

  // // Function to steer different merging prescriptions.
  // int mergeProcess( Event& process) override;

  // // Runtime interface functions for communication with aMCatNLO
  // // Function to retrieve shower scale information (to be used to set
  // // scales in aMCatNLO-LHEF-production.
  // void getStoppingInfo(double scales[100][100],
  //   double masses[100][100]) override;

  // // Function to retrieve if any of the shower scales would not have been
  // // possible to produce by Pythia.
  // void getDeadzones(bool dzone[100][100]) override;

  // // Function to generate Sudakov factors for MCatNLO-Delta.
  // double generateSingleSudakov (double pTbegAll, double pTendAll,
  //   double m2dip, int idA, int type, double s = -1.,
  //   double x = -1.) override;

  // // LHEF3FromPythia8Ptr lhaPtr;
  // void setLHAPtr(LHEF3FromPythia8Ptr lhaUpIn) override
  // {lhaPtr = lhaUpIn;}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyMerging_H
