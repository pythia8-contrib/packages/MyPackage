// MyPhaseSpace.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyPhaseSpace_H
#define MyPackage_MyPhaseSpace_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyPhaseSpace here.
// The following template declares all methods that can be inherited from
// PhaseSpace.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyPhaseSpace.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyPhaseSpace.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyPhaseSpace : public PhaseSpace {

public:

  // Constructor (defined in src/MyPhaseSpace.cc).
  MyPhaseSpace(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyPhaseSpace() = default;

  // Define an optimization procedure to determine how phase space
  // should be sampled. Return true of succesful.
  bool setupSampling() override
  {return false;}

  // Set up trial event kinematics to be selected. Return true if succesful.
  bool trialKin(bool /*inEvent = true*/, bool /*repeatSame = false*/)
    override
  {return false;}

  // A pure virtual method, wherein the accepted event kinematics
  // is to be constructed in the derived class.
  bool finalKin() override
  {return false;}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyPhaseSpace_H
