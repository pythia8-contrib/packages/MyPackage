// MyBeamShape.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyBeamShape_H
#define MyPackage_MyBeamShape_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyBeamShape here.
// The following template declares all methods that can be inherited from
// BeamShape.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyBeamShape.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyBeamShape.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyBeamShape : public BeamShape {

public:

  // Constructor (defined in src/MyBeamShape.cc).
  MyBeamShape(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyBeamShape() = default;

  // // Initialize beam parameters.
  // void init( Settings& settings, Rndm* rndmPtrIn) override;

  // // Set the two beam momentum deviations and the beam vertex.
  // void pick() override;

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyBeamShape_H
