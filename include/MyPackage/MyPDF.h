// MyPDF.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyPDF_H
#define MyPackage_MyPDF_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyPDF here.
// The following template declares all methods that can be inherited from
// PDF.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyPDF.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyPDF.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyPDF : public PDF {

public:

  // Constructor (defined in src/MyPDF.cc).
  MyPDF(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyPDF() = default;

  // // Confirm that PDF has been set up (important for LHAPDF and H1 Pomeron).
  // bool isSetup() override
  // {return isSet;}

  // // Switch to new beam particle identities; for similar hadrons only.
  // // For hadrons, beamType defines special cases and determines how
  // // to handle isospin symmetries.
  // //  1: no rearrangement (e.g. p, Sigma+, Omega-, pi+)
  // // -1: switch u <-> d (e.g. n, Sigma-, Xi-, K0)
  // //  0: take average of u and d (e.g. Sigma0, Lambda0)
  // //  2/-2: Delta++/Delta-
  // //  111: pi0-like special case (pi0, rho0, omega, etc.)
  // //  221: Other diagonal meson cases (eta, eta', phi, J/psi, Upsilon, etc.)
  // //  130: K_S,L special cases
  // void setBeamID(int idBeamIn) override
  // { idBeam = idBeamIn;
  //   idBeamAbs = abs(idBeam); idSav = 9; xSav = -1.; Q2Sav = -1.;
  //   resetValenceContent();}

  // // Allow extrapolation beyond boundaries. This is optional.
  // void setExtrapolate(bool) override
  // {}

  // // Check whether x and Q2 values fall inside the fit bounds.
  // bool insideBounds(double, double) override
  // {return true;}

  // // Access the running alpha_s of a PDF set.
  // double alphaS(double) override
  // { return 1.;}

  // // Return quark masses used in the PDF fit.
  // double mQuarkPDF(int) override
  // { return -1.;}

  // // Return number of members of this PDF family.
  // int nMembers() override
  // { return 1;}

  // // Calculate PDF envelope.
  // void calcPDFEnvelope(int, double, double, int) override
  // {}
  // void calcPDFEnvelope(pair<int,int>, pair<double,double>, double,
  //   int) override
  // {}
  // PDFEnvelope getPDFEnvelope() override
  // { return PDFEnvelope(); }

  // // Approximate photon PDFs by decoupling the scale and x-dependence.
  // double gammaPDFxDependence(int, double) override
  // { return 0.; }

  // // Provide the reference scale for logarithmic Q^2 evolution for photons.
  // double gammaPDFRefScale(int) override
  // { return 0.; }

  // // Sample the valence content for photons.
  // int sampleGammaValFlavor(double) override
  // { return 0.; }

  // // The total x-integrated PDFs. Relevant for MPIs with photon beams.
  // double xfIntegratedTotal(double) override
  // { return 0.; }

  // // Return the sampled value for x_gamma.
  // double xGamma() override
  // { return 1.; }

  // // Keep track of pomeron momentum fraction.
  // void xPom(double = -1.0) override
  // {}

  // // Return accurate and approximated photon fluxes and PDFs.
  // double xfFlux(int id, double x, double Q2) override
  // { return 0.; }
  // double xfApprox(int id, double x, double Q2) override
  // { return 0.; }
  // double xfGamma(int id, double x, double Q2) override
  // { return 0.; }
  // double intFluxApprox() override
  // { return 0.; }
  // bool hasApproxGammaFlux() override
  // { return false; }

  // // Return the kinematical limits and sample Q2 and x.
  // double getXmin() override
  // { return 0.; }
  // double getXhadr() override
  // { return 0.; }
  // double sampleXgamma(double xMinIn)  override
  // { return 0.; }
  // double sampleQ2gamma(double Q2minIN) override
  // { return 0.; }

  // // Normal PDFs unless gamma inside lepton -> an overestimate for sampling.
  // double xfMax(int id, double x, double Q2) override
  // { return xf( id, x, Q2); }

  // // Normal PDFs unless gamma inside lepton -> Do not sample x_gamma.
  // double xfSame(int id, double x, double Q2) override
  // { return xf( id, x, Q2); }

  // // Allow for new scaling factor for VMD PDFs.
  // void setVMDscale(double = 1.) override {}

protected:

  // Update parton densities (defined in src/MyPDF.cc). Must be defined.
  void xfUpdate(int id, double x, double Q2) override;

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyPDF_H
