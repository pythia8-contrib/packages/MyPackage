// MyDecayHandler.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyDecayHandler_H
#define MyPackage_MyDecayHandler_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyDecayHandler here.
// The following template declares all methods that can be inherited from
// DecayHandler.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyDecayHandler.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyDecayHandler.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyDecayHandler : public DecayHandler {

public:

  // Constructor (defined in src/MyDecayHandler.cc).
  MyDecayHandler(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyDecayHandler() = default;

  // // A virtual method, wherein the derived class method does a decay.
  // // Details in src/MyDecayHandler.cc.
  // bool decay(vector<int>& idProd, vector<double>& mProd,
  //   vector<Vec4>& pProd, int iDec, const Event& event) override;

  // // A virtual method, to do sequential decay chains.
  // // Details in src/MyDecayHandler.cc.
  // bool chainDecay(vector<int>& idProd, vector<int>& motherProd,
  //   vector<double>& mProd, vector<Vec4>& pProd, int iDec,
  //   const Event& event) override;

  // // A virtual method, to return the particles the handler should decay.
  // vector<int> handledParticles() override {return {};}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyDecayHandler_H
