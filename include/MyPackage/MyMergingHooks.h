// MyMergingHooks.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyMergingHooks_H
#define MyPackage_MyMergingHooks_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyMergingHooks here.
// The following template declares all methods that can be inherited from
// MergingHooks.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyMergingHooks.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyMergingHooks.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyMergingHooks : public MergingHooks {

public:

  // Constructor (defined in src/MyMergingHooks.cc).
  MyMergingHooks(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyMergingHooks() = default;

  // // Function encoding the functional definition of the merging scale
  // double tmsDefinition( const Event& event) override
  // {return event[0].e();}

  // // Function to dampen weights calculated from histories with lowest
  // // multiplicity reclustered events that do not pass the ME cuts
  // double dampenIfFailCuts( const Event& /*inEvent*/ ) override
  // {return 1.;}

  // // Hooks to disallow states in the construction of all histories, e.g.
  // // because jets are below the merging scale or fail the matrix element cuts
  // // Function to allow interference in the construction of histories
  // bool canCutOnRecState() override
  // { return doCutOnRecStateSave; }

  // // Function to check reclustered state while generating all possible
  // // histories.
  // bool doCutOnRecState( const Event& event ) override {
  //   // Count number of final state partons.
  //   int nPartons = 0;
  //   for( int i=0; i < int(event.size()); ++i)
  //     if(  event[i].isFinal()
  //       && (event[i].isGluon() || event[i].isQuark()) )
  //       nPartons++;
  //   // For gg -> h, allow only histories with gluons in initial state
  //   if( hasEffectiveG2EW() && nPartons < 2){
  //     if(event[3].id() != 21 && event[4].id() != 21)
  //       return true;
  //   }
  //   return false;
  // }

  // // Function to allow not counting a trial emission.
  // bool canVetoTrialEmission() override
  // { return false;}

  // // Function to check if trial emission should be rejected.
  // virtual bool doVetoTrialEmission( const Event&, const Event& )
  // { return false; }

  // // Function to calculate the hard process matrix element.
  // virtual double hardProcessME( const Event& /*inEvent*/ )
  // { return 1.; }

  // // Initialize.
  // void init();

  // // Function to return the value of the merging scale function in the
  // // current event.
  // double tmsNow( const Event& event ) override;

  // // Function to allow not counting a trial emission.
  // bool canVetoEmission() override
  // { return !doIgnoreEmissionsSave; }

  // // Function to check if emission should be rejected.
  // virtual bool doVetoEmission( const Event& ) override;

  // // Check if hooks use Vincia.
  // bool usesVincia() override
  // {return false;}

  // // Check if shower plugin should be used.
  // bool useShowerPlugin() override
  // { return useShowerPluginSave; }

  // // Function to check event veto.
  // bool doVetoStep(const Event& process, const Event& event,
  //   bool doResonance = false) override;
  // {return false;}

  // // Set starting scales.
  // bool setShowerStartingScales( bool isTrial, bool doMergeFirstEmm,
  //   double& pTscaleIn, const Event& event,
  //   double& pTmaxFSRIn, bool& limitPTmaxFSRin,
  //   double& pTmaxISRIn, bool& limitPTmaxISRin,
  //   double& pTmaxMPIIn, bool& limitPTmaxMPIin ) override;

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyMergingHooks_H
