// MyHeavyIons.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyHeavyIons_H
#define MyPackage_MyHeavyIons_H

#include "Pythia8/Pythia.h"
#include "Pythia8/HeavyIons.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyHeavyIons here.
// The following template declares all methods that can be inherited from
// HeavyIons.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyHeavyIons.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyHeavyIons.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyHeavyIons : public HeavyIons {

public:

  // Constructor (defined in src/MyHeavyIons.cc).
  MyHeavyIons(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyHeavyIons() = default;

  // Virtual function to be implemented in a subclass. This will be
  // called in the beginning of the Pythia::init function if the mode
  // HeavyIon:mode is set non zero. The return value should be true
  // if this object is able to handle the requested collision
  // type. If false Pythia::init will set HeavyIon:mode to zero but
  // will try to cope anyway.
  bool init() override
  {return false;}

  // Virtual function to be implemented in a subclass. This will be
  // called in the beginning of the Pythia::next function if
  // HeavyIon:mode is set non zero. After the call, Pythia::next will
  // return immediately with the return value of this function.
  bool next() override
  {return false;}

  // // Set beam kinematics.
  // bool setKinematics(double /*eCMIn*/) override
  // {loggerPtr->ERROR_MSG("method not implemented for this heavy ion model");
  //   return false; }
  // bool setKinematics(double /*eAIn*/, double /*eBIn*/) override
  // {loggerPtr->ERROR_MSG("method not implemented for this heavy ion model");
  //   return false; }
  // bool setKinematics(double /*pxA*/, double /*pyA*/, double /*pzA*/,
  //   double /*pxB*/, double /*pyB*/, double /*pzB*/) override
  // {loggerPtr->ERROR_MSG("method not implemented for this heavy ion model");
  //   return false; }
  // bool setKinematics(Vec4 /*pA*/, Vec4 /*pB*/) override
  // {loggerPtr->ERROR_MSG("method not implemented for this heavy ion model");
  //   return false; }

  // // Print out statistics.
  // void stat() override
  // {}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyHeavyIons_H
