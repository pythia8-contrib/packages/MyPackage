// MyUserHooks.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyUserHooks_H
#define MyPackage_MyUserHooks_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyUserHooks here.
// The following template declares all methods that can be inherited from
// UserHooks.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyUserHooks.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyUserHooks.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyUserHooks : public UserHooks {

public:

  // Constructor (defined in src/MyUserHooks.cc).
  MyUserHooks(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyUserHooks() = default;

  // // Initialisation after beams have been set by Pythia::init().
  // bool initAfterBeams() override;
  // {return true;}

  // // Possibility to modify cross section of process.
  // bool canModifySigma() override;
  // {return false;}

  // // Multiplicative factor modifying the cross section of a hard process.
  // double multiplySigmaBy(const SigmaProcess* sigmaProcessPtr,
  //   const PhaseSpace* phaseSpacePtr, bool inEvent) override;

  // // Possibility to bias selection of events, compensated by a weight.
  // bool canBiasSelection() override;
  // {return false;}

  // // Multiplicative factor in the phase space selection of a hard process.
  // double biasSelectionBy(const SigmaProcess* sigmaProcessPtr,
  //   const PhaseSpace* phaseSpacePtr, bool inEvent) override;

  // // Event weight to compensate for selection weight above.
  // double biasedSelectionWeight() override;
  // {return 1./selBias;}

  // // Possibility to veto event after process-level selection.
  // bool canVetoProcessLevel() override;
  // {return false;}

  // // Decide whether to veto current process or not, based on process record.
  /// / Usage: doVetoProcessLevel( process).
  // bool doVetoProcessLevel(Event& event) override;
  // {return false;}

  // // Possibility to veto resonance decay chain.
  // bool canVetoResonanceDecays() override;
  // {return false;}

  // // Decide whether to veto current resonance decay chain or not, based on
  // // process record. Usage: doVetoProcessLevel( process).
  // bool doVetoResonanceDecays(Event&) override;
  // {return false;}

  // // Possibility to veto MPI + ISR + FSR evolution and kill event,
  // // making decision at a fixed pT scale. Useful for MLM-style matching.
  // bool canVetoPT() override;
  // {return false;}

  // // Transverse-momentum scale for veto test.
  // double scaleVetoPT() override;
  // {return 0.;}

  // // Decide whether to veto current event or not, based on event record.
  // // Usage: doVetoPT( iPos, event), where iPos = 0: no emissions so far;
  // // iPos = 1/2/3 joint evolution, latest step was MPI/ISR/FSR;
  // // iPos = 4: FSR only afterwards; iPos = 5: FSR in resonance decay.
  // bool doVetoPT(int iPos, const Event& event) override;
  // {return false;}

  // // Possibility to veto MPI + ISR + FSR evolution and kill event,
  // // making decision after fixed number of ISR or FSR steps.
  // bool canVetoStep() override;
  // {return false;}

  // // Up to how many ISR + FSR steps of hardest interaction should be
  // // checked.
  // int numberVetoStep() override;
  // {return 1;}

  // // Decide whether to veto current event or not, based on event record.
  // // Usage: doVetoStep( iPos, nISR, nFSR, event), where iPos as above,
  // // nISR and nFSR number of emissions so far for hard interaction only.
  // bool doVetoStep((int iPos, int nISR, int nFSR,
  //     const Event& event) override;
  // {return false;}

  // // Possibility to veto MPI + ISR + FSR evolution and kill event,
  // // making decision after fixed number of MPI steps.
  // bool canVetoMPIStep() override;
  // {return false;}

  // // Up to how many MPI steps should be checked.
  // // int numberVetoMPIStep() override;
  // {return 1;}

  // // Decide whether to veto current event or not, based on event record.
  // // Usage: doVetoMPIStep( nMPI, event), where nMPI is number of MPI's so
  // // far.
  // bool doVetoMPIStep(int nMPI, const Event& event) override;
  // {return false;}

  // // Possibility to veto event after ISR + FSR + MPI in parton level,
  // // but before beam remnants and resonance decays.
  // bool canVetoPartonLevelEarly() override;
  // {return false;}

  // // Decide whether to veto current partons or not, based on event record.
  // bool doVetoPartonLevelEarly(const Event& event) override;
  // {return false;}

  // // Retry same ProcessLevel with a new PartonLevel after a veto in
  // // doVetoPT, doVetoStep, doVetoMPIStep or doVetoPartonLevelEarly
  // // if you overload this method to return true.
  // bool retryPartonLevel() override;
  // {return false;}

  // // Possibility to veto event after parton-level selection.
  // bool canVetoPartonLevel() override;
  // {return false;}

  // // Decide whether to veto current partons or not, based on event record.
  // bool doVetoPartonLevel(const Event& event) override;
  // {return false;}

  // // Possibility to set initial scale in TimeShower for resonance decay.
  // bool canSetResonanceScale() override;
  // {return false;}

  // // Initial scale for TimeShower evolution.
  // // Usage: scaleResonance( iRes, event), where iRes is location
  // // of decaying resonance in the event record.
  // double scaleResonance(int iRes, const Event& event) override;
  // {return 0.;}

  // // Possibility to veto an emission in the ISR machinery.
  // bool canVetoISREmission() override;
  // {return false;}

  // // Decide whether to veto current emission or not, based on event record.
  // // Usage: doVetoISREmission( sizeOld, event, iSys) where sizeOld is size
  // // of event record before current emission-to-be-scrutinized was added,
  // // and iSys is the system of the radiation (according to PartonSystems).
  // bool doVetoISREmission(int sizeOld, const Event& event,
  //   int iSys) override;
  // {return false;}

  // // Possibility to veto an emission in the FSR machinery.
  // bool canVetoFSREmission() override;
  // {return false;}

  // // Decide whether to veto current emission or not, based on event record.
  // // Usage: doVetoFSREmission( sizeOld, event, iSys, inResonance) where
  // // sizeOld is size of event record before current emission-to-be-
  // // scrutinized was added, iSys is the system of the radiation
  // // (according to PartonSystems), and inResonance is true if the emission
  // // takes place in a resonance decay.
  // bool doVetoFSREmission(int sizeOld, const Event& event, int iSys,
  //   bool inResonance = false) override;
  // {return false;}

  // // Possibility to veto an MPI.
  // bool canVetoMPIEmission() override;
  // {return false;}

  // // Decide whether to veto an MPI based on event record.
  // // Usage: doVetoMPIEmission( sizeOld, event) where sizeOld
  // // is size of event record before the current MPI.
  // bool doVetoMPIEmission(int sizeOld, const Event& event) override;
  // {return false;}

  // // Possibility to reconnect colours from resonance decay systems.
  // bool canReconnectResonanceSystems() override;
  // {return false;}

  // // Do reconnect colours from resonance decay systems.
  // // Usage: doVetoFSREmission( oldSizeEvt, event)
  // // where oldSizeEvent is the event size before resonance decays.
  // // Should normally return true, while false means serious failure.
  // // Value of PartonLevel:earlyResDec determines where method is called.
  // bool doReconnectResonanceSystems(int oldSizeEvent, Event &event) override;
  // {return true;}

  // // Can change fragmentation parameters.
  // bool canChangeFragPar() override;
  // {return false;}

  // // Set initial ends of a string to be fragmented. This is done once
  // // for each string. Note that the second string end may be zero in case
  // // we are hadronising a string piece leading to a junction.
  // void setStringEnds(const StringEnd*, const StringEnd*,
  //   vector<int>) override;
  // {}

  // // Do change fragmentation parameters.
  // // Input: flavPtr, zPtr, pTPtr, idEnd, m2Had, iParton and posEnd (or
  // // negEnd).
  // bool doChangeFragPar(StringFlav* flavPtr, StringZ* zPtr,
  //   StringPT* pTPtr, int idEnd, double m2Had, vector<int> iParton,
  //   const StringEnd* end) override;
  // {return false;}

  // // Do a veto on a hadron just before it is added to the final state.
  // // The StringEnd from which the the hadron was produced is included
  // // for information.
  // bool doVetoFragmentation(Particle had, const StringEnd* end) override;
  // {return false;}

  // // Do a veto on a hadron just before it is added to the final state
  // // (final two hadron case).
  // bool doVetoFragmentation(Particle had1, Particle had2,
  //   const StringEnd* end1, const StringEnd* end2) override;
  // {return false;}

  // // Possibility to veto an event after hadronization based
  // // on event contents. Works as an early trigger to avoid
  // // running the time consuming rescattering process on
  // // uninteresting events.
  // bool canVetoAfterHadronization() override;
  // {return false;}

  // // Do the actual veto after hadronization.
  // bool doVetoAfterHadronization(const Event& event) override;
  // {return false;}
  
  // // Can set the overall impact parameter for the MPI treatment.
  // bool canSetImpactParameter() const override;
  // {return false;}

  // // Set the overall impact parameter for the MPI treatment.
  // double doSetImpactParameter() override;
  // {return 0.0;}
  
  // // Custom processing at the end of HadronLevel::next.
  // bool onEndHadronLevel(HadronLevel& hadLevel, Event& event) override;
  // {return true;}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyUserHooks_H
