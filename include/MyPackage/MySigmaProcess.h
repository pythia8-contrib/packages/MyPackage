// MySigmaProcess.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MySigmaProcess_H
#define MyPackage_MySigmaProcess_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MySigmaProcess here.
// The following template declares all methods that can be inherited from
// SigmaProcess.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MySigmaProcess.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MySigmaProcess.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MySigmaProcess : public SigmaProcess {

public:

  // Constructor (defined in src/MySigmaProcess.cc).
  MySigmaProcess(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MySigmaProcess() = default;

  // // Initialize process. Only used for some processes.
  // void initProc() override
  // {}

  // // Set up allowed flux of incoming partons. Default is no flux.
  // bool initFlux() override;

  // // Input and complement kinematics for resolved 2 -> 1 process.
  // void set1Kin( double x1In, double x2In, double sHIn) override
  // {}

  // // Input and complement kinematics for resolved 2 -> 2 process.
  // void set2Kin( double x1In, double x2In, double sHIn, double tHIn,
  //   double m3In, double m4In, double runBW3In, double runBW4In)
  // {}

  // // Ditto, but for Multiparton Interactions applications, so different
  // // input.
  // void set2KinMPI( double x1In, double x2In, double sHIn, double tHIn,
  //   double uHIn, double alpSIn, double alpEMIn, bool needMasses, double m3In,
  //   double m4In) override
  // {}

  // // Input and complement kinematics for resolved 2 -> 3 process.
  // // Usage: set3Kin( x1in, x2in, sHin, p3prel, p4prel, p5prel,
  // //                 m3in, m4in, m5in, runBW3in, runBW4in, runBW5in);
  // void set3Kin( double x1In, double x2In, double sHIn, Vec4 p3cmIn,
  //   Vec4 p4cmIn, Vec4 p5cmIn, double m3In, double m4In, double m5In,
  //   double runBW3In, double runBW4In, double runBW5In) override
  // {}

  // // Calculate flavour-independent parts of cross section.
  // void sigmaKin() override
  // {}

  // // Evaluate sigma for unresolved, sigmaHat(sHat) for 2 -> 1 processes,
  // // d(sigmaHat)/d(tHat) for (resolved) 2 -> 2 processes, and |M|^2 for
  // // 2 -> 3 processes. Answer in "native" units, either mb or GeV^-2.
  // double sigmaHat()
  // {return 0.;}

  // // Wrapper to sigmaHat, to (a) store current incoming flavours and
  // // (b) convert from GeV^-2 to mb where required.
  // // For 2 -> 1/2 also (c) convert from from |M|^2 to d(sigmaHat)/d(tHat).
  // double sigmaHatWrap(int id1in = 0, int id2in = 0) override {
  //   id1 = id1in; id2 = id2in;
  //   return ( convert2mb() ? CONVERT2MB * sigmaHat() : sigmaHat() ); }

  // // Convolute above with parton flux and K factor. Sum over open channels.
  // // Possibly different PDF in initialization phase or no sampling for
  // // x_gamma (photons in leptons).
  // double sigmaPDF(bool initPS = false, bool samexGamma = false,
  //   bool useNewXvalues = false, double x1New = 0.,
  //   double x2New = 0.) override;

  // // Select flavour, colour and anticolour.
  // void setIdColAcol() override
  // {}

  // // Perform kinematics for a Multiparton Interaction, in its rest frame.
  // bool final2KinMPI(int i1Res = 0, int i2Res = 0, Vec4 p1Res = 0,
  //   Vec4 p2Res = 0, double m1Res = 0, double m2Res = 0) override
  // {return true;}

  // // Evaluate weight for simultaneous flavours (only gamma*/Z0 gamma*/Z0).
  // double weightDecayFlav( Event& process) override
  // {return 1.;}

  // // Evaluate weight for decay angular configuration.
  // // iResBeg <= i < iResEnd is range of sister partons to test decays of.
  // double weightDecay( Event& process, int iResBeg, int iResEnd) override
  // {return 1.;}

  // // Set scale, when that is missing for an external LHA process.
  // void setScale() override
  // {}

  // // Process name.
  // string name() const override
  // {return "unnamed process";}

  // // Process code.
  // int code() const override
  // {return 0;}

  // // Number of final-state particles.
  // int nFinal() const override
  // {return 2;}

  // // Need to know which incoming partons to set up interaction for.
  // // This string specifies the combinations of incoming partons that
  // // are allowed for the process under consideration, and thereby
  // // which incoming flavours id1 and id2 the sigmaHat() calls will be
  // // looped over. It is always possible to pick a wider flavour
  // // selection than strictly required and then put to zero cross
  // // sections in the superfluous channels, but of course this may cost
  // // some extra execution time. Currently allowed options are:
  // // * gg: two gluons.
  // // * qg: one (anti)quark and one gluon.
  // // * qq: any combination of two quarks, two antiquarks or a quark and
  // //       an antiquark.
  // // * qqbar: any combination of a quark and an antiquark; a subset of the
  // //          qq option.
  // // * qqbarSame: a quark and its antiquark; a subset of the qqbar option.
  // // * ff: any combination of two fermions, two antifermions or a fermion
  // //       and an antifermion; is the same as qq for hadron beams but also
  // //       allows processes to work with lepton beams.
  // // * ffbar: any combination of a fermion and an antifermion; is the same
  // //          as qqbar for hadron beams but also allows processes to work
  // //          with lepton beams.
  // // * ffbarSame: a fermion and its antifermion; is the same as qqbarSame for
  // //              hadron beams but also allows processes to work with lepton
  // //              beams.
  // // * ffbarChg: a fermion and an antifermion that combine to give
  // //             charge +-1.
  // // * fgm: a fermion and a photon (gamma).
  // // * ggm: a gluon and a photon.
  // // * gmgm: two photons. 
  // string inFlux() const override
  // {return "unknown";}

  // // Need to know whether to convert cross section answer from GeV^-2 to mb.
  // bool convert2mb() const override
  // {return true;}

  // // For 2 -> 2 process optional conversion from |M|^2 to
  // // d(sigmaHat)/d(tHat).
  // bool convertM2() const override
  // {return false;}

  // // Special treatment needed for Les Houches processes.
  // bool isLHA() const override
  // {return false;}

  // // Specify if a non-diffractive process.
  // bool isNonDiff() const override
  // {return false;}

  // // Specify if beams are resolved or scatter directly.
  // bool isResolved() const override
  // {return true;}

  // // Beam A is soft difractively excited.
  // bool isDiffA() const override
  // {return false;}

  // // Beam B is soft difractively excited.
  // bool isDiffB() const override
  // {return false;}

  // // Specify central diffractive process (double Pomeron exchange).
  // bool isDiffC() const override
  // {return false;}

  // // Special treatment needed for SUSY processes.
  // bool isSUSY() const override
  // {return false;}

  // // Special treatment needed if negative cross sections allowed.
  // bool allowNegativeSigma() const override
  // {return false;}

  // // Flavours in 2 -> 2/3 processes where masses needed from beginning.
  // // (For a light quark masses will be used in the final kinematics,
  // // but not at the matrix-element level. For a gluon no masses at all.)
  // int id3Mass() const override
  // {return 0;}
  // int id4Mass() const override
  // {return 0;}
  // int id5Mass() const override
  // {return 0;}

  // // Special treatment needed if process contains an s-channel
  // // resonance. These are the codes of up to two s-channel resonances
  // // contributing to the matrix elements. These are used by the
  // // program to improve the phase-space selection efficiency, by
  // // partly sampling according to the relevant Breit-Wigner
  // // distributions. Massless resonances (the gluon and photon) need
  // // not be specified.
  // int resonanceA() const override
  // {return 0;}
  // int resonanceB() const override
  // {return 0;}

  // // 2 -> 2 and 2 -> 3 processes only through s-channel exchange.
  // bool isSChannel() const override
  // {return false;}

  // // Insert an intermediate resonance in 2 -> 1 -> 2 (or 3) listings.
  // // Normally no intermediate state is shown in the event record for 2
  // // -> 2 and 2 -> 3 processes. However, in case that idSChannel is
  // // overloaded to return a nonzero value, an intermediate particle
  // // with that identity code is inserted into the event record, to
  // // make it a 2 -> 1 -> 2 or 2 -> 1 -> 3 process. Thus if both
  // // isSChannel and idSChannel are overloaded, a process will behave
  // // and look like it proceeded through a resonance. The one
  // // difference is that the implementation of the matrix element is
  // // not based on the division into a production and a decay of an
  // // intermediate resonance, but is directly describing the transition
  // // from the initial to the final state.
  // int idSChannel() const override
  // {return 0;}

  // // QCD 2 -> 3 processes need special phase space selection
  // // machinery.  There are two different 3-body phase-space selection
  // // machineries, of which the non-QCD one is default. If you overload
  // // this method instead the QCD-inspired machinery will be used. The
  // // differences between these two is related to which phase space
  // // cuts can be set, and also that the QCD machinery assumes (almost)
  // // massless outgoing partons.
  // bool isQCD3body() const
  // {return false;}

  // // The non-QCD 2 -> 3 phase space selection machinery is rather
  // // primitive, as already mentioned. The efficiency can be improved
  // // in processes that proceed though t-channel exchanges, such as q
  // // qbar' -> H^0 q qbar' via Z^0 Z^0 fusion, if the identity of the
  // // t-channel-exchanged particles on the two side of the event are
  // // provided. Only the absolute value is of interest.
  // int idTchan1() const overload
  // {return 0;}
  // int idTchan2() const overload
  // {return 0;}

  // // In the above kind of 2 -> 3 phase-space selection, the sampling
  // // of pT^2 is done with one part flat, one part weighted like 1 /
  // // (pT^2 + m_R^2) and one part like 1 / (pT^2 + m_R^2)^2. The above
  // // values provide the relative amount put in the latter two
  // // channels, respectively, with the first obtaining the rest. Thus
  // // the sum of tChanFracPow1() and tChanFracPow2() must be below
  // // unity. The final results should be independent of these numbers,
  // // but the Monte Carlo efficiency may be quite low for a bad
  // // choice. Here m_R is the mass of the exchanged resonance specified
  // // by idTchan1() or idTchan2(). Note that the order of the
  // // final-state listing is important in the above q qbar' -> H^0 q
  // // qbar' example, i.e. the H^0 must be returned by id3Mass(), since
  // // it is actually the pT^2 of the latter two that are selected
  // // independently, with the first pT then fixed by
  // // transverse-momentum conservation.
  // double tChanFracPow1() const override
  // {return 0.3;}
  // double tChanFracPow2() const override
  // {return 0.3;}

  // // In 2 -> 3 processes the phase space selection used here involves
  // // a twofold ambiguity basically corresponding to a flipping of the
  // // positions of last two outgoing particles. These are assumed
  // // equally likely by default, false, but for processes proceeding
  // // entirely through t-channel exchange the Monte Carlo efficiency
  // // can be improved by making a preselection based on the relative
  // // propagator weights, true.
  // bool useMirrorWeight() const override
  // {return false;}

  // // Special process-specific gamma*/Z0 choice if >=0 (e.g. f fbar
  // // -> H0 Z0).
  // // Allows a possibility to override the global mode WeakZ0:gmZmode
  // // for a specific process. The global mode normally is used to
  // // switch off parts of the gamma^*/Z^0 propagator for test
  // // purposes. The above local mode is useful for processes where a
  // // Z^0 really is that and nothing more, such as q qbar -> H^0
  // // Z^0. The default value -1 returned by gmZmode() ensures that the
  // // global mode is used, while 0 gives full gamma^*/Z^0 interference,
  // // 1 gamma^* only and 2 Z^0 only.
  // int gmZmode() const override
  // {return -1;}

  // // Set the incoming ids for diffraction.
  // void setIdInDiff(int, int) override
  // {}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MySigmaProcess_H
