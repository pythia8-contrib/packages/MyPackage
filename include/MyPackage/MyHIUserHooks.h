// MyHIUserHooks.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyHIUserHooks_H
#define MyPackage_MyHIUserHooks_H

#include "Pythia8/Pythia.h"
#include "Pythia8/HIInfo.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyHIUserHooks here.
// The following template declares all methods that can be inherited from
// HIUserHooks.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyHIUserHooks.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyHIUserHooks.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyHIUserHooks : public HIUserHooks {

public:

  // Constructor (defined in src/MyHIUserHooks.cc).
  MyHIUserHooks(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyHIUserHooks() = default;

  // // Initialize this user hook.
  // void init(int idProjIn, int idTargIn) override
  // {
  //   idProjSave = idProjIn;
  //   idTargSave = idTargIn;
  // }

  // // A user-supplied impact parameter generator.
  // bool hasImpactParameterGenerator() const override
  // { return false; }
  // ImpactParameterGenerator* impactParameterGenerator() const override
  // { return nullptr; }

  // // A user-supplied NucleusModel for the projectile and target.
  // bool hasProjectileModel() const override
  // { return false; }
  // NucleusModel* projectileModel() const override
  // { return nullptr; }
  // bool hasTargetModel() const override
  // { return false; }
  // NucleusModel* targetModel() const override
  // { return nullptr; }

  // // A user-supplied SubCollisionModel for generating nucleon-nucleon
  // // subcollisions.
  // bool hasSubCollisionModel() override
  // { return false; }
  // SubCollisionModel* subCollisionModel() override
  // { return nullptr; }

  // // A user-supplied ordering of events in (inverse) hardness.
  // bool hasEventOrdering() const override
  // { return false; }
  // double eventOrdering(const Event &, const Info &) override
  // { return -1; }

  // // A user-supplied method for fixing up proton-neutron mismatch in
  // // generated beams.
  // bool canFixIsoSpin() const override
  // { return false; }
  // bool fixIsoSpin(EventInfo &) override
  // { return false; }

  // // A user-supplied method for shifting the event
  // // in impact parameter space.
  // bool canShiftEvent() const override
  // { return false; }
  // EventInfo & shiftEvent(EventInfo & ei) const override
  // { return ei; }

  // // A user-supplied method of adding a diffractive excitation event
  // // to another event, optionally connecting their colours.
  // bool canAddNucleonExcitation() const override
  // { return false; }
  // bool addNucleonExcitation(EventInfo &, EventInfo &, bool) const override
  // { return false; }

  // // A user supplied wrapper around the Pythia::forceHadronLevel()
  // bool canForceHadronLevel() const override
  // { return false; }
  // bool forceHadronLevel(Pythia &) override
  // { return false; }

  // // A user-supplied way of finding the remnants of an
  // // non-diffrcative pp collision (on the target side if tside is
  // // true) to be used to give momentum when adding.
  // bool canFindRecoilers() const override
  // { return false; }
  // vector<int> findRecoilers(const Event &, bool /* tside */, int /* beam */,
  //   int /* end */, const Vec4 & /* pdiff */,
  //   const Vec4 & /* pbeam */) const override
  // { return {}; }

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyHIUserHooks_H
