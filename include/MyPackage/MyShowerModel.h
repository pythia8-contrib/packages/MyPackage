// MyShowerModel.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyShowerModel_H
#define MyPackage_MyShowerModel_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyShowerModel here.
// The following template declares all methods that can be inherited from
// ShowerModel.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyShowerModel.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyShowerModel.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyShowerModel : public ShowerModel {

public:

  // Constructor (defined in src/MyShowerModel.cc).
  MyShowerModel(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyShowerModel() = default;


  // Function called from Pythia after the basic pointers has been set.
  // Derived classes should create objects of the specific model objects
  // to be used. Pointers to merging and merging hooks may be overwritten
  // in derived classes.
  bool init(MergingPtr mergPtrIn, MergingHooksPtr mergHooksPtrIn,
    PartonVertexPtr partonVertexPtrIn,
    WeightContainer* weightContainerPtrIn) override;

  // Function called from Pythia after the beam particles have been set up,
  // so that showers may be initialized after the beams are initialized.
  bool initAfterBeams() override
  {return true;}

  // // Access the pointers to the different model components.
  // TimeShowerPtr getTimeShower() const override
  // { return timesPtr; }

  // TimeShowerPtr getTimeDecShower() const override
  // { return timesDecPtr; }

  // SpaceShowerPtr getSpaceShower() const override
  // { return spacePtr; }

  // MergingPtr getMerging() const override
  // { return mergingPtr; }

  // MergingHooksPtr getMergingHooks() const override 
  // { return mergingHooksPtr; }

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyShowerModel_H
