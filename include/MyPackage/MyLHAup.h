// MyLHAup.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyLHAup_H
#define MyPackage_MyLHAup_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyLHAup here.
// The following template declares all methods that can be inherited from
// LHAup.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyLHAup.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyLHAup.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyLHAup : public LHAup {

public:

  // Constructor (defined in src/MyLHAup.cc).
  MyLHAup(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyLHAup() = default;

  // // Method to be used for LHAupLHEF derived class.
  // void newEventFile(const char*) override
  // {}
  // bool fileFound() override
  // {return true;}
  // bool useExternal() override
  // {return false;}

  // This method must be defined. A pure virtual method setInit,
  // wherein all initialization information is supposed to be set in
  // the derived class. Can do this by reading a file or some other
  // way, as desired. Returns false if it did not work.
  bool setInit() override {return false;}

  // This method must be defined (example defined in src/MyLHAup.cc). A
  // pure virtual method setEvent, wherein information on the next
  // event is supposed to be set in the derived class. Strategies +-1
  // and +-2: idProcIn is the process type, selected by
  // PYTHIA. Strategies +-3 and +-4: idProcIn is dummy; process choice
  // is made locally.  The method can find the next event by a runtime
  // interface to another program, or by reading a file, as
  // desired. The method should return false if it did not work.
  bool setEvent(int idProcIn = 0) override;

  // // Skip ahead a number of events, which are not considered further.
  // // Mainly intended for debug when using the LHAupLHEF class.
  // bool skipEvent(int nSkip) override {
  //   for (int iSkip = 0; iSkip < nSkip; ++iSkip)
  //     if (!setEvent()) return false;
  //   return true;}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyLHAup_H
