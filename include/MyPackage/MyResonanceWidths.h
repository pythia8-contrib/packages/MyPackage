// MyResonanceWidths.h is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef MyPackage_MyResonanceWidths_H
#define MyPackage_MyResonanceWidths_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace MyPackage {

//==========================================================================

// Add documentation about the class MyResonanceWidths here.
// The following template declares all methods that can be inherited from
// ResonanceWidths.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/MyResonanceWidths.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/MyResonanceWidths.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class MyResonanceWidths : public ResonanceWidths {

public:

  // Constructor (defined in src/MyResonanceWidths.cc).
  MyResonanceWidths(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~MyResonanceWidths() = default;

  // // Calculate and store partial and total widths at the nominal mass.
  // bool init(Info* infoPtrIn) override;


  // // Initialize constants.
  // // Is called once during initialization, and can then be used to set
  // // up further parameters specific to this particle species, such as
  // // couplings, and perform calculations that need not be repeated for
  // // each new event, thereby saving time. This method needs not be
  // // implemented.
  // void initConstants() override
  // {}

  // // Can normally be left dummy, but for advanced implementations it
  // // provides a possibility to initialize data members of the derived
  // // class at a very early stage during initialization, before any of
  // // the other members are called. An example is provided by the
  // // SUSYResonanceWidths class, in which an internal pointer to a
  // // derived Couplings class must be (re)set before any of the other
  // // methods are used. A return value of false can be used to signal
  // // that this initialization step failed.
  // bool initBSM() override
  // {return true;}

  // // Can normally be left dummy (and then always returns true) but can
  // // optionally be used to determine whether to force dynamical width
  // // calculation to be switched off (return false). An example is
  // // provided by the SUSYResonanceWidths class, in which the
  // // implementation of this method checks for the existence of SLHA
  // // decay tables for the particular resonance in question, and checks
  // // if those tables should be given precedence over the internal
  // // width calculation.
  // bool allowCalc() override
  // {return true;}

  // // Is called once a mass has been chosen for the resonance, but
  // // before a specific final state is considered. This routine can
  // // therefore be used to perform calculations that otherwise might
  // // have to be repeated over and over again in calcWidth below. It is
  // // optional whether you want to use this method, however, or put
  // // everything in calcWidth(). The optional argument will have the
  // // value true when the resonance is initialized, and then be false
  // // throughout the event generation, should you wish to make a
  // // distinction. In PYTHIA such a distinction is made for gamma^*/Z^0
  // // and gamma^*/Z^0/Z'^0, owing to the necessity of a special
  // // description of interference effects, but not for other
  // // resonances. In addition to the base-class member variables
  // // already described above, mHat contains the current mass of the
  // // resonance. At initialization this agrees with the nominal mass
  // // mRes, but during the run it will not (in general).
  // void calcPreFac(bool calledFromInit = false) override
  // {}

  // Calculate width for currently considered channel.  Optional
  // argument calledFromInit only used for Z0.  This is the key method
  // for width calculations and returns a partial width value, as
  // further described below. It is called for a specific final state,
  // typically in a loop over all allowed final states, subsequent to
  // the calcPreFac(...) call above. Information on the final state is
  // stored in a number of base-class variables, for you to use in
  // your calculations: iChannel : the channel number in the list of
  // possible decay channels;
  // mult : the number of decay products;

  // // id1, id2, id3 : the identity code of up to the first three decay
  // // products, arranged in descending order of the absolute value of
  // // the identity code;
  // // id1Abs, id2Abs, id3Abs : the absolute value of the above three
  // // identity codes;
  // // mHat : the current resonance mass, which is the same as in the
  // // latest calcPreFac(...) call;
  // // mf1, mf2, mf3 : masses of the above decay products;
  // // mr1, mr2, mr3 : squared ratio of the product masses to the
  // // resonance mass;
  // // ps : is only meaningful for two-body decays, where it gives the
  // // phase-space factor ps = sqrt( (1. - mr1 - mr2)^2 - 4. * mr1 * mr2
  // // );
  // // In two-body decays the third slot is zero for the above
  // // properties. Should there be more than three particles in the
  // // decay, you would have to take care of the subsequent products
  // // yourself, e.g. using particlePtr->decay[iChannel].product(j); to
  // // extract the j'th decay products (with j = 0 for the first,
  // // etc.). Currently we are not aware of any such examples.
  // // The base class also contains methods for alpha_em and
  // // alpha_strong evaluation, and can access many standard-model
  // // couplings; see the existing code for examples.
  // // The result of your calculation should be stored in
  // // widNow : the partial width of the current channel, expressed in
  // // GeV.
  // void calcWidth(bool calledFromInit = false) override
  // {}

};

//==========================================================================

} // end namespace MyPackage

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // MyPackage_MyResonanceWidths_H
