# Makefile is a part of the PYTHIA-CONTRIB package MyPackage.
# Copyright (C) 2023 AUTHORS.
# MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.

# This is is the Makefile used to build MyPackage on POSIX systems.
# Example usage is:
#     make -j2
# For help using the make command please consult the local system documentation,
# i.e. "man make" or "make --help".

################################################################################
# VARIABLES: Definition of the relevant variables from the configuration script.
################################################################################

# Set the shell.
SHELL=/usr/bin/env bash

# Include the MyPackage configuration.
-include Makefile.inc

# Local directory structure.
LOCAL_INCLUDE=include
LOCAL_SRC=src
LOCAL_TMP=tmp
LOCAL_LIB=lib
LOCAL_MKDIRS:=$(shell mkdir -p $(LOCAL_TMP) $(LOCAL_LIB))

# MyPackage.
OBJECTS=$(patsubst $(LOCAL_SRC)/%.cc,$(LOCAL_TMP)/%.o,\
	$(sort $(wildcard $(LOCAL_SRC)/*.cc)))
TARGETS=$(LOCAL_LIB)/libMyPackage.so

# Compiler flags.
CXX_COMMON:=-I$(LOCAL_INCLUDE) $(PYTHIA8_INCLUDE) $(CXX_COMMON)
OBJ_COMMON:=-MD $(CXX_COMMON) $(OBJ_COMMON)

################################################################################
# RULES: Definition of the rules used to build MyPackage.
################################################################################

# Rules without physical targets (secondary expansion for specific rules).
.SECONDEXPANSION:
.PHONY: all contrib clean

# All targets.
all: $(TARGETS)

# Copy MyPackage to PYTHIA-CONTRIB after building.
contrib: $(LOCAL_LIB)/libMyPackage.so
	cp $< ../lib/
	cp -r include/MyPackage ../include/Pythia8Contrib/
	cp -r share/MyPackage ../share/Pythia8Contrib/

# Clean.
clean:
	rm -rf $(LOCAL_TMP) $(LOCAL_LIB)

# The Makefile configuration.
Makefile.inc:
	./configure

# Auto-generated (with -MD flag) dependencies.
-include $(LOCAL_TMP)/*.d

# MyPackage.
$(LOCAL_TMP)/%.o: $(LOCAL_SRC)/%.cc
	$(CXX) $< -o $@ -c $(OBJ_COMMON)
$(LOCAL_LIB)/libMyPackage.so: $(OBJECTS)
	$(CXX) $^ -o $@ $(CXX_COMMON) $(CXX_SHARED) $(CXX_SONAME)$(notdir $@)

# If external dependencies are necessary, their paths will be read in
# from "Makefile.inc". A check then can be made if the external package
# has been succesfully configured as follows.

# <target>: <prerequisites> 
# ifeq ($(DEP_USE),true)
# 	<recipe, given the package exists>
# else
# 	<recipe, given the package does not exist, or, error message>
# 	$(error Error: $@ requires DEP)
# endif

# The following variables will have been defined for the external package
# with name "DEP" from "Makefile.inc".
# DEP_USE: true if the package is succesfully enabled, false otherwise.
# DEP_CONFIG: the configure script.
# DEP_BIN: the path to the binary directory for the package.
# DEP_INCLUDE: the header include flags.
# DEP_LIB: the library flags used for linking, both paths and libraries.

# For the MyPackage plugin library with a dependency, the library rule
# can be changed as follows.

# ifeq ($(DEP_USE),true)
# $(LOCAL_LIB)/libMyPackage.so: $(OBJECTS)
# 	$(CXX) $^ -o $@ $(CXX_COMMON) $(CXX_SHARED) $(CXX_SONAME)$(notdir $@)\
# 	 $(DEP_INCLUDE) $(DEP_LIB)
# else
# 	 $(error Error: $@ requires DEP)
# endif

# There is some special treatment that needs to be handled when
# linking against RIVET. The following fixes missing runtime paths
# from RIVET and determines the C++ standard used by RIVET.

# ifeq ($(RIVET_USE),true)
#   COMMA=,
#   RIVET_LPATH=$(filter -L%,$(shell $(RIVET_BIN)$(RIVET_CONFIG) --ldflags))
#   RIVET_RPATH=$(subst -L,-Wl$(COMMA)-rpath$(COMMA),$(RIVET_LPATH))
#   RIVET_FLAGS=-DRIVET
#   RIVET_VERSION=$(shell $(RIVET_BIN)$(RIVET_CONFIG) --version)
#   RIVET_CSTD=c++14
#   ifeq ("4.0.0","$(word 1, $(sort 4.0.0 $(RIVET_VERSION)))")
#     RIVET_CSTD=c++17
#   endif
#   DTAGS=$(shell echo "int main(){}" | $(CXX) -Wl,--disable-new-dtags -o \
#      /dev/null -xc - 2>&1)
#   ifeq ($(DTAGS),)
#     RIVET_FLAGS+= -Wl,--disable-new-dtags
#   endif
# endif

# The rule defined below ensures the correct C++ standard is used.

# ifeq ($(RIVET_USE),true)
# 	$(CXX) $@.cc -o $@ -w $(CXX_COMMON:c++11=$(RIVET_CSTD)) $(RIVET_FLAGS)\
# 	 $(shell $(RIVET_BIN)$(RIVET_CONFIG) --cppflags --libs) $(RIVET_RPATH)
# else
