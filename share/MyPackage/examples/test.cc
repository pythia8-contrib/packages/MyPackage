// test.cc is a part of the PYTHIA-CONTRIB package MyPackage.
// Copyright (C) 2023 AUTHORS.
// MyPackage is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage

// This is a simple test program for the plugin classes of
// MyPackage. Any number of command files can be passed as command line
// arguments.

#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main(int argc, char** argv) {
  Pythia pythia;
  pythia.readString("ProcessLevel:all = off");
  for (int iArg = 1; iArg < argc; ++iArg)
    pythia.readFile(argv[iArg]);
  pythia.init();
  return 0;
}
